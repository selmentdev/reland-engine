# Naming Convention:

## Files

- naming: `snake_case`
- modules: `snake_case`
- subdirectories: `snake_case`

example:

```
reland/base/string.hxx
```

## Includes

- naming: snake_case
- includes: within `<>`

example:

```c++
#include <reland/base/string.hxx>
```

## Types and namespaces

- naming: snake_case
- interfaces: like pure abstract classes
- base classes: use `_base` suffix
- template parameters: PascalCase
- use namespace fully qualified names

example:

```c++
namespace reland::subsystem
{
    // typedef alias
    template <typename T>
    using gpu_handle = T*;

    // value type
    struct entity final
    {
        uint64_t index;
    };

    // base interface-alike class
    struct message_handler_base
    {
        virtual ~message_handler_base() noexcept = default;

        virtual void on_click(int in_count) noexcept = 0;
    };
}
```

## Variables names

- naming: `snake_case`
- global variables: `g_some_variable`
- local variables: `some_variable`
- parameters: `%direction%_%name%`
- no hungarian notation in any form

```c++
namespace reland
{
    class CORE_API task_dispatcher final
    {
    public:
        static task_dispatcher& instance() noexcept;
        
    public:
        void start(size_t in_threads) noexcept;
    };

    extern task_dispatcher* g_task_dispatcher;
}
```

## Functions

- use global `to_string` and `from_string` instead of member functions.

```c++
namespace reland
{
    bool to_string(std::string& out_result, const version& in_value) noexcept;
    bool from_string(version& out_result, std::string_view value) noexcept;
}
```

- preffer free functions in math library

```c++
namespace reland::math
{
    struct vector4 { };
    struct vector3 { };
    struct vector2 { };
    struct quaternion { };

    // shortest names
    vector4 add(vector4 v1, vector4 v2) noexcept;
    vector4 dot(vector4 v1, vector4 v2) noexcept;

    // constructor functions
    vector4 to_vector4(float value) noexcept;
    vector4 to_vector4(float x, float y, float z, float w) noexcept;

    // prefix type name to functions which cannot be distincted by arguments.
    vector4 vector4_load(float4* components) noexcept;  // 4 -> 4
    vector4 vector4_load(float3* components) noexcept;  // 3 -> 4
    vector4 vector4_load(float2* components) noexcept;  // 2 -> 4
    vector3 vector3_load(float3* components) noexcept;  // 3 -> 3
    vector3 vector3_load(float2* components) noexcept;  // 2 -> 3
    vector2 vector2_load(float2* components) noexcept;  // 2 -> 2

    vector4 vector4_load_aligned(float4a* components) noexcept;
    
    // mix math functions for scalars and vectors in order to use them in templated code
    vector4 sin(vector4 v) noexcept;
    float sin(float v) noexcept;
}
```