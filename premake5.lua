solution "Reland Engine"

    configurations {
        "debug",
        "profile",
        "release"
    }

    platforms {
        "static",
        "shared",
        --"static-windows-x64",
        --"shared-windows-x64",
        --"static-windows-arm64",
        --"shared-windows-arm64",
        --"static-android-arm64",
    }

    flags {
        "MultiProcessorCompile",
        "FatalWarnings",
        --"NoFramePointer",
        --"NoRuntimeChecks",
        --"LinkTimeOptimization"
    }

    language "C++"
    architecture "x64"
	objdir "%{wks.location}/obj/%{cfg.buildcfg}"
    targetdir "%{wks.location}/bin/%{cfg.buildcfg}-%{cfg.platform}"
    systemversion "latest"
    warnings "extra"
    rtti "off"
    stringpooling "on"
    --strictaliasing "level3"
    pic "on"
    nativewchar "on"
    --largeadressaware "on"
    intrinsics "on"
    inlining "auto"
    functionlevellinking "on"
    floatingpoint "fast"
    endian "little"
    editorintegration "on"
    startproject "game"
    exceptionhandling "off"

--    filter { "platforms:*-x64" }
--        architecture "x64"
--
--    filter { "platforms:*-arm64" }
--        architecture "arm64"
--
--    filter { "platforms:*-windows-*" }
--        system "windows"
--
--    filter { "platforms:*-linux-*" }
--        system "linux"
--
--    filter { "platforms:*-android-*" }
--        system "android"
    
    filter { "system:windows" }
        toolset "msc"

    filter { "system:linux" }
        toolset "clang"

    filter { "system:android" }
        toolset "clang"
        --androidAPILevel "17"
        --androidStlType "llvm libc++ shared"

    filter { "language:C++" }
        cppdialect "C++17"

    filter { "language:C" }
        cdialect "C11"

    defines {
        "__STDC_WANT_LIB_EXT1__=1",
        "__STDINT_MACROS",
        "__STDINT_LIMITS",
        "__STDC_CONSTANT_MACROS",
        "__STDC_FORMAT_MACROS",
        "__STDC_LIMIT_MACROS",
    }

    filter { "architecture:x86*" }
        vectorextensions "avx"

    filter { "architecture:arm64" }
        editandcontinue "off"

    filter { "toolset:msc*" }
        exceptionhandling "SEH"
        cppdialect "c++latest"
        buildoptions {
            "/permissive-"
        }
        linkoptions {
            "/ignore:4221", -- LNK4221: This object file does not define any previously undefined public symbols, so it will not be used by any link operation that consumes this library
        }
        defines {
            "_HAS_EXCEPTIONS=0",
        }

    filter { "toolset:msc*", "configurations:release" }
        defines {
			"_HAS_ITERATOR_DEBUGGING=0",
			"_SCL_SECURE=0",
			"_SECURE_SCL=0",
        }
        buildoptions {
            "/sdl-",
        }
    
    filter "configurations:debug"
        defines { "DEBUG" }
        symbols "On"

    filter "configurations:release"
        defines { "NDEBUG" }
        optimize "Full"

    filter "platforms:static*"
        kind "StaticLib"
        defines { "RELAND_STATIC_BUILD=1" }
    
    filter "platforms:shared*"
        kind "SharedLib"
        defines { "RELAND_STATIC_BUILD=0" }


group "engine"
    include "engine"

group "game"
    include "game"

group "sdk"
    include "sdk"

function configfunction()
    local ver = os.getversion()
    local os_version = string.format("%d.%d.%d", ver.majorversion, ver.minorversion, ver.revision, ver.description)
    local os_host = os.host()
    local build_commit = os.outputof('git log -1 --format=%H')
    local build_commit_short = os.outputof('git log -1 --format=%h')
    local build_branch = os.outputof('git rev-parse --abbrev-ref HEAD')

    local date = os.date('*t')

    local build_version_major = date.year - 2000
    local build_version_minor = date.month
    local build_version_release = date.yday
    local build_version_build = tonumber((date.yday * 60 * 24 + date.hour * 60 + date.min) >> 4)

    local build_version = string.format('%d.%d.%d.%d',
        build_version_major,
        build_version_minor,
        build_version_release,
        build_version_build
    )

    return string.format(
        '#define RELAND_BUILD_OS_VERSION        "%s"\n' ..
        '#define RELAND_BUILD_OS_HOST           "%s"\n' ..
        '#define RELAND_BUILD_COMMIT            "%s"\n' ..
        '#define RELAND_BUILD_COMMIT_SHORT      "%s"\n' ..
        '#define RELAND_BUILD_BRANCH            "%s"\n' ..
        '#define RELAND_BUILD_UUID              "%s"\n' ..
        '#define RELAND_BUILD_VERSION           "%s"\n' ..
        '#define RELAND_BUILD_VERSION_MAJOR     %d\n' ..
        '#define RELAND_BUILD_VERSION_MINOR     %d\n' ..
        '#define RELAND_BUILD_VERSION_RELEASE   %d\n' ..
        '#define RELAND_BUILD_VERSION_BUILD     %d\n',
        os_version,
        os_host,
        build_commit or "<unknown>",
        build_commit_short or "<unknown>",
        build_branch or "<unknown>",
        os.uuid('graphyte'):gsub('-', ''):lower(),
        build_version,
        build_version_major,
        build_version_minor,
        build_version_release,
        build_version_build
    );
end

io.writefile('engine/include/reland/build.version.hxx', configfunction())

filter { "action:vs*" }
    group "solution"
        project "common-files"
            kind "utility"
            files {
                "premake5.lua",
                "engine/include/**",
                "engine/resources/**",
                "generate.*",
            }
