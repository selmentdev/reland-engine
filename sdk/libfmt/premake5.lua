project "fmt"
    language "C++"
    files {
        "include/**.h",
        "source/**.cc",
        "**.lua"
    }
    includedirs {
        "%{wks.location}/sdk/libfmt/include"
    }
    defines {
        "FMT_EXCEPTIONS=0",
    }

    filter { "kind:SharedLib" }
        defines {
            "FMT_EXPORT=1",
        }
