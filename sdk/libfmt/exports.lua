filter { "platforms:shared" }
    defines {
        "FMT_SHARED=1",
    }

filter {}
    defines {
        "FMT_EXCEPTIONS=0",
    }
    links {
        "fmt",
    }
    includedirs {
        "%{wks.location}/sdk/libfmt/include"
    }
