project "lua"
    language "C"
    files {
        "include/**.h",
        "source/**.c",
        "**.lua"
    }
    includedirs {
        "./include",
    }

    filter { "kind:SharedLib", "toolset:msc*" }
        defines {
            "LUA_BUILD_AS_DLL=1",
        }

    filter { "toolset:msc*" }
        disablewarnings {
            "4244",
            "4324",
            "4310",
            "4702",
        }
        
    filter { "system:linux" }
        defines {
            "LUA_USE_LINUX=1"
        }
        links {
            "dl",
            "m",
        }
