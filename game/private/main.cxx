#include <fmt/format.h>
#include <lua.hpp>
#include <reland/core/span.hxx>
#include <reland/entities/entity_manager.hxx>

#if __has_include(<reland/build.version.hxx>)
#include <reland/build.version.hxx>
#else
#define RELAND_BUILD_OS_VERSION    "<unknown>"
#define RELAND_BUILD_OS_HOST       "<unknown>"
#define RELAND_BUILD_COMMIT        "<unknown>"
#define RELAND_BUILD_COMMIT_SHORT  "<unknown>"
#define RELAND_BUILD_BRANCH        "<unknown>"
#define RELAND_BUILD_UUID          "<unknown>"
#endif

CORE_API void nonunique_function();

template <typename T>
struct TestPermissive final
{
    using Type = T;
};

template <typename T>
struct TestPermissive2 final
{
    using Type = typename TestPermissive<T>::Type;
};

int main()
{
#if _HAS_EXCEPTIONS
    fmt::print("exceptions\n");
#else
    fmt::print("no-exceptions\n");
#endif
    reland::entities::entity_manager manager{};
    fmt::print("has entity: {0}\n", manager.has_entity(reland::entities::entity{}));
    
    fmt::print("RELAND_BUILD_OS_VERSION:    {0}\n", RELAND_BUILD_OS_VERSION);
    fmt::print("RELAND_BUILD_OS_HOST:       {0}\n", RELAND_BUILD_OS_HOST);
    fmt::print("RELAND_BUILD_COMMIT:        {0}\n", RELAND_BUILD_COMMIT);
    fmt::print("RELAND_BUILD_COMMIT_SHORT:  {0}\n", RELAND_BUILD_COMMIT_SHORT);
    fmt::print("RELAND_BUILD_BRANCH:        {0}\n", RELAND_BUILD_BRANCH);
    fmt::print("RELAND_BUILD_UUID:          {0}\n", RELAND_BUILD_UUID);

    fmt::print("{0}-{1}-{2}-{3}-{4}\n",
        RELAND_COMPILER_NAME,
        RELAND_PLATFORM_NAME,
        RELAND_CPU_NAME,
        RELAND_ARCH_NAME,
        RELAND_CRT_NAME
    );
    fmt::print("RELAND_HW_AVX:   {0}\n", static_cast<bool>(RELAND_HW_AVX));
    fmt::print("RELAND_HW_AVX2:  {0}\n", static_cast<bool>(RELAND_HW_AVX2));
    fmt::print("RELAND_HW_SSE:   {0}\n", static_cast<bool>(RELAND_HW_SSE));
    fmt::print("RELAND_HW_SSE2:  {0}\n", static_cast<bool>(RELAND_HW_SSE2));
    fmt::print("RELAND_HW_NEON:  {0}\n", static_cast<bool>(RELAND_HW_NEON));
    fmt::print("RELAND_HW_FMA3:  {0}\n", static_cast<bool>(RELAND_HW_FMA3));
    fmt::print("RELAND_HW_FMA4:  {0}\n", static_cast<bool>(RELAND_HW_FMA4));
    fmt::print("RELAND_HW_F16C:  {0}\n", static_cast<bool>(RELAND_HW_F16C));
    fmt::print("RELAND_HW_AESNI: {0}\n", static_cast<bool>(RELAND_HW_AESNI));
    fmt::print("RELAND_HW_SHA:   {0}\n", static_cast<bool>(RELAND_HW_SHA));
    fmt::print("RELAND_HW_QPX:   {0}\n", static_cast<bool>(RELAND_HW_QPX));
    fmt::print("RELAND_HW_VMX:   {0}\n", static_cast<bool>(RELAND_HW_VMX));
    fmt::print("RELAND_HW_VSX:   {0}\n", static_cast<bool>(RELAND_HW_VSX));

    TestPermissive2<uint8_t>::Type d = 20;
    std::puts(fmt::format("hello {0} {0} {0}", "world").c_str());
    std::puts(fmt::format("hello {0} {0} {1}", "world", d).c_str());
    nonunique_function();
    lua_State* state = luaL_newstate();
    luaL_openlibs(state);

    std::printf("> ");

    std::array<char, 256> buffer{};

    while (std::fgets(std::data(buffer), static_cast<int>(std::size(buffer)), stdin) != nullptr)
    {
        int error = luaL_loadstring(state, std::data(buffer)) || lua_pcall(state, 0, 0, 0);

        if (error != 0)
        {
            std::fprintf(stderr, "%s\n", lua_tostring(state, -1));
            lua_pop(state, 1);
        }

        std::printf("> ");
    }

    lua_close(state);
    return 0;
}
