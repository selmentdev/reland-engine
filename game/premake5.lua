project "game"
    kind "ConsoleApp"
    language "C++"
    files {
        "public/**.hxx",
        "private/**.cxx",
        "**.lua",
        --"%{wks.location}/engine/resources/*.manifest",
    }
    includedirs {
        "%{wks.location}/engine/include",
        "./public",
    }

    dofile "engine/runtime/core/exports.lua"
    dofile "engine/runtime/entities/exports.lua"
    dofile "sdk/liblua/exports.lua"
    dofile "sdk/libfmt/exports.lua"
