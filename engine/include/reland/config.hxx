

#if false
#pragma once

//
// Compiler information.
//

#define RELAND_COMPILER              "gnu"

#if defined(__clang__)
#define RELAND_COMPILER_CLANG 1
#endif

#if defined(__GCC__)
#define RELAND_COMPILER_GNU 1
#endif

#if defined(__intel__)
#define RELAND_COMPILER_INTEL 1
#endif

#if defined(_MSC_VER)
#define RELAND_COMPILER_MSVC 1
#endif

//
// Version information.
//

#define RELAND_ENGINE_VERSION_MAJOR       1
#define RELAND_ENGINE_VERSION_MINOR       0
#define RELAND_ENGINE_VERSION_PATCH       0
#define RELAND_ENGINE_VERSION_BUILD       0
#define RELAND_ENGINE_VERSION_STRING      "1.0.0.0"

#define BUILD_PROJECT_COMPANY_NAME          "Graphyte"
#define BUILD_PROJECT_COPYRIGHT_STRING      "Copyright 2010-2018 Karol Grzybowski"
#define BUILD_PROJECT_PRODUCT_NAME          "Graphyte Engine"
#define BUILD_PROJECT_PRODUCT_IDENTIFIER    "linux-x86_64-develop-1.0.0.0"


//
// Build information.
//

#define RELAND_BUILD_OS              "Linux-4.15.0-43-generic"
#define RELAND_BUILD_OS_NAME         "Linux"

//#define RELAND_STATIC_BUILD 0

#define RELAND_ENDIAN_LITTLE 1

#define RELAND_BUILD_TYPE            "develop"
#define RELAND_BUILD_TYPE_DEVELOP 1
#define RELAND_BUILD_TYPE_QA 0
#define RELAND_BUILD_TYPE_RETAIL 0


//
// Target architecture.
//

#define RELAND_ARCHITECTURE          "x86_64"

#define RELAND_ARCHITECTURE_ARMV7A 0
#define RELAND_ARCHITECTURE_AARCH64 0
#define RELAND_ARCHITECTURE_X86 0
#define RELAND_ARCHITECTURE_AMD64 1


//
// Target platform.
//

#define RELAND_PLATFORM              "linux"
#define RELAND_PLATFORM_OSX 0
#define RELAND_PLATFORM_ANDROID 0
#if defined(_WIN32)
#define RELAND_PLATFORM_WINDOWS 1
#define RELAND_PLATFORM_LINUX 0
#else
#define RELAND_PLATFORM_WINDOWS 0
#define RELAND_PLATFORM_LINUX 1
#endif
#define RELAND_PLATFORM_BSD 0
#define RELAND_PLATFORM_POSIX 0
#define RELAND_PLATFORM_WINDOWS_STORE 0


#define RELAND_PLATFORM_TYPE         "desktop"
#define RELAND_PLATFORM_TYPE_DESKTOP 1
#define RELAND_PLATFORM_TYPE_MOBILE 0
#define RELAND_PLATFORM_TYPE_CONSOLE 0

#define RELAND_PLATFORM_HAS_TOUCH_SCREEN 0
#define RELAND_PLATFORM_HAS_SEH_EXCEPTIONS 0

//
// Profiler settings.
//

#define RELAND_ENABLE_PROFILE 1


//
// Assert configurations.
//

#define RELAND_CONFIG_DO_ASSERT 1
#define RELAND_CONFIG_DO_VERIFY 1
#define RELAND_CONFIG_DO_ENSURE 1


//
// Math intrinsics
//

#define RELAND_MATH_NO_INTRINSICS 1
#define RELAND_MATH_FAST_FUNCTIONS 0
#define RELAND_MATH_ENABLE_OPERATORS 1

//
// Platform features.
//

#define RELAND_HAVE_VECTORCALL 0
#define RELAND_HAVE_EXCEPTIONS 0
#define RELAND_HAVE_UNALIGNED_ACCESS 1

#define RELAND_HAVE_AVX 1
#define RELAND_HAVE_AVX2 0

#define RELAND_HAVE_FMA3 0
#define RELAND_HAVE_FMA4 0
#define RELAND_HAVE_F16C 1

#define RELAND_HAVE_AESNI 0
#define RELAND_HAVE_SHA 0

#define RELAND_HAVE_NEON 0

#define RELAND_HAVE_PTHREADS 1


//
// Custom options
//

#define RELAND_ENTITIES_ENABLE_CHECKS 1
#endif
