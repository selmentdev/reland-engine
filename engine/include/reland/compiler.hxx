#pragma once

//
// Preprocessor token support macros.
//

#define __cxx_token_paste_impl(x, y)        x ## y
#define __cxx_token_concat(x, y)            __cxx_token_paste_impl(x, y)

#define __cxx_token_string_impl(x)          #x
#define __cxx_token_string(x)               __cxx_token_string_impl(x)

#define __cxx_unique_name(prefix)           __cxx_token_concat(prefix, __LINE__)

#define __cxx_nameof(symbol)                __cxx_token_string_impl(symbol)


//
// Noinline attribute.
//

#if defined(_MSC_VER)
#define __cxx_noinline                  __declspec(noinline)
#else
#define __cxx_noinline                  __attribute__((__noinline__))
#endif


//
// Forceinline attribute.
//

#if !(defined(_MSC_VER) || defined(__MINGW32__) || defined(__MINGW64__))
#define __forceinline                   inline __attribute__((always_inline))
#endif


//
// Unreachable declarator.
//

#if defined(_MSC_VER)
#define __cxx_unreachable()
#else
#define __cxx_unreachable()             __builtin_unreachable()
#endif


//
// Assume
//

#if defined(_MSC_VER)
#define __cxx_assume(expression)        __analysis_assume(!!(expression))
#else
#define __cxx_assume(expression)
#endif


//
// String format attribute.
//
#if defined(_MSC_VER)
#define __cxx_format_printf(format, args)
#define __cxx_format_scanf(format, args)
#define __cxx_format_strftime(format)
#define __cxx_format_strfmon(format, args)
#else
#define __cxx_format_printf(format, args)                   [[gnu::format(printf, format, args)]]
#define __cxx_format_scanf(format, args)                    [[gnu::format(scanf, format, args)]]
#define __cxx_format_strftime(format)                       [[gnu::format(strftime, format, 0)]]
#define __cxx_format_strfmon(format, args)                  [[gnu::format(strfmon, format, args)]]
#define __c_decl_format_attribute(type, format, args)       [[gnu::format(type, format, args)]]
#endif


//
// Debugger break.
//

#if defined(_MSC_VER)
#define __cxx_debugbreak                __debugbreak
#else
#define __cxx_debugbreak                __builtin_trap
#endif


//
// Export / import.
//

#if defined(_MSC_VER)
#define __cxx_lib_export                __declspec(dllexport)
#define __cxx_lib_import                __declspec(dllimport)
#else
#define __cxx_lib_export                __attribute__((visibility("default")))
#define __cxx_lib_import                __attribute__((visibility("default")))
#endif


//
// Likely / unlikely.
//

#if defined(_MSC_VER)
#define __cxx_likely(expression)        (expression)
#define __cxx_unlikely(expression)      (expression)
#else
#define __cxx_likely(expression)        __builtin_expect(!!(expression), 1)
#define __cxx_unlikely(expression)      __builtin_expect(!!(expression), 0)
#endif


//
// Vector Align.
//

#define __cxx_vector_align              alignas(16)


//
// Threadlocal variable.
//

#if defined(_MSC_VER)
#define __cxx_threadlocal               __declspec(thread)
#else
#define __cxx_threadlocal               __thread
#endif


//
// Intrin type.
//

#if defined(_MSC_VER)
#define __cxx_intrintype                __declspec(intrin_type)
#else
#define __cxx_intrintype
#endif

//
// Select any.
//

#if defined(_MSC_VER)
#define __cxx_selectany                 __declspec(selectany)
#else
#define __cxx_selectany
#endif

#if defined(_MSC_VER)
#pragma inline_depth (255)
#endif

#if defined(_MSC_VER)
#define __cxx_packed                    __declspec(align(1))
#else
#define __cxx_packed                    __attribute__((__packed__))
#endif


//
// Enable bit operations for specified enum type.
//
#define RX_ENUM_CLASS_FLAGS(enum_type) \
    inline enum_type& operator |= (enum_type& lhs, const enum_type rhs) noexcept { return lhs = static_cast<enum_type>(static_cast<std::underlying_type_t<enum_type>>(lhs) | static_cast<std::underlying_type_t<enum_type>>(rhs)); } \
    inline enum_type& operator &= (enum_type& lhs, const enum_type rhs) noexcept { return lhs = static_cast<enum_type>(static_cast<std::underlying_type_t<enum_type>>(lhs) & static_cast<std::underlying_type_t<enum_type>>(rhs)); } \
    inline enum_type& operator ^= (enum_type& lhs, const enum_type rhs) noexcept { return lhs = static_cast<enum_type>(static_cast<std::underlying_type_t<enum_type>>(lhs) & static_cast<std::underlying_type_t<enum_type>>(rhs)); } \
    inline constexpr enum_type operator | (const enum_type lhs, const enum_type rhs) noexcept { return static_cast<enum_type>(static_cast<std::underlying_type_t<enum_type>>(lhs) | static_cast<std::underlying_type_t<enum_type>>(rhs)); } \
    inline constexpr enum_type operator & (const enum_type lhs, const enum_type rhs) noexcept { return static_cast<enum_type>(static_cast<std::underlying_type_t<enum_type>>(lhs) & static_cast<std::underlying_type_t<enum_type>>(rhs)); } \
    inline constexpr enum_type operator ^ (const enum_type lhs, const enum_type rhs) noexcept { return static_cast<enum_type>(static_cast<std::underlying_type_t<enum_type>>(lhs) ^ static_cast<std::underlying_type_t<enum_type>>(rhs)); } \
    inline constexpr bool operator ! (const enum_type value) noexcept { return !static_cast<std::underlying_type_t<enum_type>>(value); } \
    inline constexpr enum_type operator ~ (const enum_type value) noexcept { return static_cast<enum_type>(~static_cast<std::underlying_type_t<enum_type>>(value)); }

//
// Make non-copyable type.
//
#define RX_MAKE_NONCOPYABLE_TYPE(type) \
public: \
    type(const type&) = delete; \
    type& operator = (const type&) = delete

//
// Make static class.
//
#define RX_MAKE_STATIC_CLASS(type) \
public: \
    type() = delete; \
    type(const type&) = delete; \
    type& operator = (const type&) = delete

