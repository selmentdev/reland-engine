#pragma once
#include <reland/platform.hxx>
#include <reland/compiler.hxx>

// http://open-std.org/JTC1/SC22/WG21/docs/papers/2016/p0482r0.html
// char8_t as unsigned char and separate type

using char8_t = unsigned char;


//
// Basic IEEE754 float types.
//
namespace reland
{
    struct ieee754_float final
    {
        union
        {
            uint32_t as_uint;
            int32_t as_int;
            float value;
        };
    };
    struct ieee754_double final
    {
        union
        {
            uint64_t as_uint;
            int64_t as_int;
            double value;
        };
    };
}
