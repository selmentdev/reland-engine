#pragma once

// Preprocessor token concatenation
#define RELAND_TOKEN_CONCAT_IMPL(x, y)          x ## y
#define RELAND_TOKEN_CONCAT(x, y)               RELAND_TOKEN_CONCAT_IMPL(x, y)

// Preprocessor token stringize
#define RELAND_TOKEN_STRING_IMPL(x)             #x
#define RELAND_TOKEN_STRING(x)                  RELAND_TOKEN_STRING_IMPL(x)

// Unique name
#define RELAND_UNIQUE_NAME(prefix)              RELAND_TOKEN_CONCAT(prefix, __LINE__)

// Architecture
#define RELAND_ARCH_32BIT 0
#define RELAND_ARCH_64BIT 0
#define RELAND_ARCH_128BIT 0

// CPU
#define RELAND_CPU_ARM_64 0
#define RELAND_CPU_ARM_32 0
#define RELAND_CPU_X86_64 0
#define RELAND_CPU_X86_32 0
#define RELAND_CPU_MIPS_64 0
#define RELAND_CPU_MIPS_32 0
#define RELAND_CPU_PPC_64 0
#define RELAND_CPU_PPC_32 0
#define RELAND_CPU_RISCV_128 0
#define RELAND_CPU_RISCV_64 0
#define RELAND_CPU_RISCV_32 0


// Endianess
#define RELAND_ENDIAN_LITTLE 0
#define RELAND_ENDIAN_BIG 0

// CPU properties
#define RELAND_CACHELINE_SIZE 0
#define RELAND_UNALIGNED_ACCESS 0

// Hardware CPU extensions
#define RELAND_HW_AVX 0
#define RELAND_HW_AVX2 0
#define RELAND_HW_SSE 0
#define RELAND_HW_SSE2 0
#define RELAND_HW_NEON 0
#define RELAND_HW_FMA3 0
#define RELAND_HW_FMA4 0
#define RELAND_HW_F16C 0
#define RELAND_HW_AESNI 0
#define RELAND_HW_SHA 0
#define RELAND_HW_QPX 0
#define RELAND_HW_VMX 0
#define RELAND_HW_VSX 0

// Compiler
#define RELAND_COMPILER_MSVC 0
#define RELAND_COMPILER_CLANG 0
#define RELAND_COMPILER_CLANG_ANALYZER 0
#define RELAND_COMPILER_GCC 0

// Compiler traits
#define RELAND_HAVE_VECTORCALL 0

// C Runtime Library
#define RELAND_CRT_MSVC 0
#define RELAND_CRT_MINGW 0
#define RELAND_CRT_GLIBC 0
#define RELAND_CRT_NEWLIB 0
#define RELAND_CRT_BIONIC 0
#define RELAND_CRT_MUSL 0
#define RELAND_CRT_LIBCXX 0
#define RELAND_CRT_NONE 0

// Platform
#define RELAND_PLATFORM_ANDROID 0
#define RELAND_PLATFORM_BSD 0
#define RELAND_PLATFORM_EMSCRIPTEN 0
#define RELAND_PLATFORM_IOS 0
#define RELAND_PLATFORM_LINUX 0
#define RELAND_PLATFORM_NX 0
#define RELAND_PLATFORM_OSX 0
#define RELAND_PLATFORM_PS4 0
#define RELAND_PLATFORM_RPI 0
#define RELAND_PLATFORM_STEAMLINK 0
#define RELAND_PLATFORM_WINDOWS 0
#define RELAND_PLATFORM_XBOXONE 0


// Detect compiler
#if defined(__clang__)
#   undef  RELAND_COMPILER_CLANG
#   define RELAND_COMPILER_CLANG (__clang_major__ * 10000 + __clang_minor__ * 100 + __clang_patchlevel__)
#   if defined(__clang_analyzer__)
#       undef  RELAND_COMPILER_CLANG_ANALYZER
#       define RELAND_COMPILER_CLANG_ANALYZER 1
#   endif
#elif defined(_MSC_VER)
#   undef RELAND_COMPILER_MSVC
#   define RELAND_COMPILER_MSVC _MSC_VER
#elif defined(__GNUC__)
#   undef RELAND_COMPILER_GCC
#   define RELAND_COMPILER_GCC (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#else
    #error "Cannot detect RELAND_COMPILER_*
#endif

// Detect architecture and CPU properties
#if defined(__aarch64__) || defined(_M_ARM64)
#   undef  RELAND_CPU_ARM_64
#   define RELAND_CPU_ARM_64 1
#   undef  RELAND_ARCH_64BIT
#   define RELAND_ARCH_64BIT 1
#   undef  RELAND_CACHELINE_SIZE
#   define RELAND_CACHELINE_SIZE 64
#elif defined(__arm__) || defined(_M_ARM)
#   undef  RELAND_CPU_ARM_32
#   define RELAND_CPU_ARM_32 1
#   undef  RELAND_ARCH_32BIT
#   define RELAND_ARCH_32BIT 1
#   undef  RELAND_CACHELINE_SIZE
#   define RELAND_CACHELINE_SIZE 64
#elif defined(__x86_64__) || defined(_M_AMD64)
#   undef  RELAND_CPU_X86_64
#   define RELAND_CPU_X86_64 1
#   undef  RELAND_ARCH_64BIT
#   define RELAND_ARCH_64BIT 1
#   undef  RELAND_CACHELINE_SIZE
#   define RELAND_CACHELINE_SIZE 64
#elif defined(__i386__) || defined(_M_IX86)
#   undef  RELAND_CPU_X86_32
#   define RELAND_CPU_X86_32 1
#   undef  RELAND_ARCH_32BIT
#   define RELAND_ARCH_32BIT 1
#   undef  RELAND_CACHELINE_SIZE
#   define RELAND_CACHELINE_SIZE 64
#elif defined(__mips64)
#   undef  RELAND_CPU_MIPS_64
#   define RELAND_CPU_MIPS_64 1
#   undef  RELAND_ARCH_64BIT
#   define RELAND_ARCH_64BIT 1
#   undef  RELAND_CACHELINE_SIZE
#   define RELAND_CACHELINE_SIZE 64
#elif defined(__MIPSEL__) || defined(__mips_isa_rev)
#   undef  RELAND_CPU_MIPS_32
#   define RELAND_CPU_MIPS_32 1
#   undef  RELAND_ARCH_32BIT
#   define RELAND_ARCH_32BIT 1
#   undef  RELAND_CACHELINE_SIZE
#   define RELAND_CACHELINE_SIZE 64
#elif defined(__powerpc64__)
#   undef  RELAND_CPU_PPC_64
#   define RELAND_CPU_PPC_64 1
#   undef  RELAND_ARCH_64BIT
#   define RELAND_ARCH_64BIT 1
#   undef  RELAND_CACHELINE_SIZE
#   define RELAND_CACHELINE_SIZE 128
#elif defined(__powerpc__) || defined(_M_PPC)
#   undef  RELAND_CPU_PPC_32
#   define RELAND_CPU_PPC_32 1
#   undef  RELAND_ARCH_32BIT
#   define RELAND_ARCH_32BIT 1
#   undef  RELAND_CACHELINE_SIZE
#   define RELAND_CACHELINE_SIZE 128
#elif defined(__riscv) || defined(__riscv__) || defined(RISCVEL)
#   undef  RELAND_CPU_RISCV
#   define RELAND_CPU_RISCV 1
#   undef  RELAND_CACHELINE_SIZE
#   define RELAND_CACHELINE_SIZE 64
#   if defined(__riscv32)
#       undef  RELAND_ARCH_32BIT
#       define RELAND_ARCH_32BIT 1
#   elif defined(__riscv64)
#       undef  RELAND_ARCH_64BIT
#       define RELAND_ARCH_64BIT 1
#   elif defined(__riscv128)
#       undef  RELAND_ARCH_128BIT
#       define RELAND_ARCH_128BIT 1
#   else
#       error "Cannot determine bitness of RISCV"
#   endif
#else
#   error "Cannot detect CPU and architecture"
#endif

// Detect endianess
#if RELAND_CPU_PPC_32 || RELAND_CPU_PPC_64
#   if _LITTLE_ENDIAN
#       undef  RELAND_ENDIAN_LITTLE
#       define RELAND_ENDIAN_LITTLE 1
#   else
#       undef  RELAND_ENDIAN_BIG
#       define RELAND_ENDIAN_BIG 1
#   endif
#else
#   undef  RELAND_ENDIAN_LITTLE
#   define RELAND_ENDIAN_LITTLE 1
#endif

// Detect platform
#if defined(_WIN32) || defined(_WIN64)
#   undef  RELAND_PLATFORM_WINDOWS
#   define RELAND_PLATFORM_WINDOWS 1
#elif defined(__ANDROID__)
#   undef  RELAND_PLATFORM_ANDROID
#   define RELAND_PLATFORM_ANDROID 1
#elif defined(__STEAMLINK__)
#   undef  RELAND_PLATFORM_STEAMLINK
#   define RELAND_PLATFORM_STEAMLINK 1
#elif defined(__VCCOREVER__)
#   undef  RELAND_PLATFORM_RPI
#   define RELAND_PLATFORM_RPI 1
#elif defined(__linux__)
#   undef  RELAND_PLATFORM_LINUX
#   define RELAND_PLATFORM_LINUX 1
#elif defined(__ENVIRONMENT_IPHONE_OS_VERSION_MIN_REQUIRED__) || defined(__ENVIRONMENT_TV_OS_VERSION_MIN_REQUIRED__)
#	undef  RELAND_PLATFORM_IOS
#	define RELAND_PLATFORM_IOS 1
#elif defined(__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__)
#	undef  RELAND_PLATFORM_OSX
#	define RELAND_PLATFORM_OSX __ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__
#elif defined(__EMSCRIPTEN__)
#	undef  RELAND_PLATFORM_EMSCRIPTEN
#	define RELAND_PLATFORM_EMSCRIPTEN 1
#elif defined(__ORBIS__)
#	undef  RELAND_PLATFORM_PS4
#	define RELAND_PLATFORM_PS4 1
#elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__) || defined(__NetBSD__) || defined(__OpenBSD__) || defined(__DragonFly__)
#	undef  RELAND_PLATFORM_BSD
#	define RELAND_PLATFORM_BSD 1
#elif defined(__NX__)
#	undef  RELAND_PLATFORM_NX
#	define RELAND_PLATFORM_NX 1
#elif defined(_DURANGO) || defined(_XBOX_ONE)
#   undef  RELAND_PLATFORM_XBOXONE
#   define RELAND_PLATFORM_XBOXONE 1
#else
#   error "Cannot detect platform"
#endif

// Detect if platform is posix compatible
#define RELAND_PLATFORM_POSIX   (0 \
    || RELAND_PLATFORM_ANDROID \
    || RELAND_PLATFORM_BSD \
    || RELAND_PLATFORM_EMSCRIPTEN \
    || RELAND_PLATFORM_IOS \
    || RELAND_PLATFORM_LINUX \
    || RELAND_PLATFORM_NX \
    || RELAND_PLATFORM_OSX \
    || RELAND_PLATFORM_PS4 \
    || RELAND_PLATFORM_RPI \
    || RELAND_PLATFORM_STEAMLINK)

// Detect if we support pthreads
#define RELAND_HAVE_PTHREADS    (0 \
    || RELAND_PLATFORM_ANDROID \
    || RELAND_PLATFORM_BSD \
    || RELAND_PLATFORM_EMSCRIPTEN \
    || RELAND_PLATFORM_IOS \
    || RELAND_PLATFORM_LINUX \
    || RELAND_PLATFORM_OSX \
    || RELAND_PLATFORM_RPI \
    || RELAND_PLATFORM_STEAMLINK)


// Detect CRT
#if !RELAND_CRT_NONE
#   if defined(__BIONIC__)
#       undef  RELAND_CRT_BIONIC
#       define RELAND_CRT_BIONIC 1
#   elif defined(_MSC_VER)
#       undef  RELAND_CRT_MSVC
#       define RELAND_CRT_MSVC 1
#   elif defined(__GLIBC__)
#       undef  RELAND_CRT_GLIBC
#       define RELAND_CRT_GLIBC (__GLIBC__ * 10000 + __GLIBC_MINOR__ * 100)
#   elif defined(__MINGW32__) || defined(__MINGW64__)
#       undef  RELAND_CRT_MINGW
#       define RELAND_CRT_MINGW 1
#	elif defined(__apple_build_version__) || defined(__ORBIS__) || defined(__EMSCRIPTEN__) || defined(__llvm__)
#       undef  RELAND_CRT_LIBCXX
#       define RELAND_CRT_LIBCXX 1
#   else
#       undef  RELAND_CRT_NONE
#       define RELAND_CRT_NONE 1
#   endif
#endif

// Detect compiler name

#if RELAND_COMPILER_MSVC
#	if RELAND_COMPILER_MSVC >= 1910 // Visual Studio 2017
#		define RELAND_COMPILER_NAME "MSVC 15.0"
#	elif RELAND_COMPILER_MSVC >= 1900 // Visual Studio 2015
#		define RELAND_COMPILER_NAME "MSVC 14.0"
#	elif RELAND_COMPILER_MSVC >= 1800 // Visual Studio 2013
#		define RELAND_COMPILER_NAME "MSVC 12.0"
#	elif RELAND_COMPILER_MSVC >= 1700 // Visual Studio 2012
#		define RELAND_COMPILER_NAME "MSVC 11.0"
#	elif RELAND_COMPILER_MSVC >= 1600 // Visual Studio 2010
#		define RELAND_COMPILER_NAME "MSVC 10.0"
#	elif RELAND_COMPILER_MSVC >= 1500 // Visual Studio 2008
#		define RELAND_COMPILER_NAME "MSVC 9.0"
#	else
#		define RELAND_COMPILER_NAME "MSVC"
#   endif
#elif RELAND_COMPILER_CLANG
#	define RELAND_COMPILER_NAME "Clang " RELAND_TOKEN_STRING(__clang_major__) "." RELAND_TOKEN_STRING(__clang_minor__) "." RELAND_TOKEN_STRING(__clang_patchlevel__)
#elif RELAND_COMPILER_GCC
#	define RELAND_COMPILER_NAME "GCC " RELAND_TOKEN_STRING(__GNUC__) "." RELAND_TOKEN_STRING(__GNUC_MINOR__) "." RELAND_TOKEN_STRING(__GNUC_PATCHLEVEL__)
#endif

// Detect platform name
#if RELAND_PLATFORM_ANDROID
#   define RELAND_PLATFORM_NAME "Android " RELAND_TOKEN_STRING(RELAND_PLATFORM_ANDROID)
#elif RELAND_PLATFORM_BSD
#   define RELAND_PLATFORM_NAME "BSD"
#elif RELAND_PLATFORM_EMSCRIPTEN
#   define RELAND_PLATFORM_NAME "asm.js " RELAND_TOKEN_STRING(__EMSCRIPTEN_major__) "." RELAND_TOKEN_STRING(__EMSCRIPTEN_minor__) "." RELAND_TOKEN_STRING(__EMSCRIPTEN_tiny__)
#elif RELAND_PLATFORM_IOS
#   define RELAND_PLATFORM_NAME "iOS"
#elif RELAND_PLATFORM_LINUX
#   define RELAND_PLATFORM_NAME "Linux"
#elif RELAND_PLATFORM_NX
#   define RELAND_PLATFORM_NAME "NX"
#elif RELAND_PLATFORM_OSX
#   define RELAND_PLATFORM_NAME "OSX"
#elif RELAND_PLATFORM_PS4
#   define RELAND_PLATFORM_NAME "PlayStation 4"
#elif RELAND_PLATFORM_RPI
#   define RELAND_PLATFORM_NAME "Raspberry PI"
#elif RELAND_PLATFORM_STEAMLINK
#   define RELAND_PLATFORM_NAME "SteamLink"
#elif RELAND_PLATFORM_WINDOWS
#   define RELAND_PLATFORM_NAME "Windows"
#elif RELAND_PLATFORM_XBOXONE
#   define RELAND_PLATFORM_NAME "Xbox One"
#endif

// Detect CPU name

#if RELAND_CPU_ARM_64
#   define RELAND_CPU_NAME "ARM64"
#elif RELAND_CPU_ARM_32
#   define RELAND_CPU_NAME "ARM"
#elif RELAND_CPU_X86_32
#   define RELAND_CPU_NAME "X86-32"
#elif RELAND_CPU_X86_64
#   define RELAND_CPU_NAME "X86-64"
#elif RELAND_CPU_MIPS_32
#   define RELAND_CPU_NAME "MIPS"
#elif RELAND_CPU_MIPS_64
#   define RELAND_CPU_NAME "MIPS64"
#elif RELAND_CPU_PPC_32
#   define RELAND_CPU_NAME "PowerPC"
#elif RELAND_CPU_PPC_64
#   define RELAND_CPU_NAME "PowerPC64"
#elif RELAND_CPU_RISCV_32
#   define RELAND_CPU_NAME "RISC-V-32"
#elif RELAND_CPU_RISCV_64
#   define RELAND_CPU_NAME "RISC-V-64"
#elif RELAND_CPU_RISCV_128
#   define RELAND_CPU_NAME "RISC-V-128"
#endif

// Detect bitness name

#if RELAND_ARCH_32BIT
#   define RELAND_ARCH_NAME "32-bit"
#elif RELAND_ARCH_64BIT
#   define RELAND_ARCH_NAME "64-bit"
#elif RELAND_ARCH_128BIT
#   define RELAND_ARCH_NAME "128-bit"
#endif

// Detect CRT name

#if RELAND_CRT_MSVC
#   define RELAND_CRT_NAME "MSVC C Runtime"
#elif RELAND_CRT_MINGW
#   define RELAND_CRT_NAME "MinGW C Runtime"
#elif RELAND_CRT_GLIBC
#   define RELAND_CRT_NAME "GNU C Library"
#elif RELAND_CRT_NEWLIB
#   define RELAND_CRT_NAME "newlib"
#elif RELAND_CRT_BIONIC
#   define RELAND_CRT_NAME "Bionic libc"
#elif RELAND_CRT_MUSL
#   define RELAND_CRT_NAME "musl libc"
#elif RELAND_CRT_LIBCXX
#   define RELAND_CRT_NAME "Clang C library"
#elif RELAND_CRT_NONE
#   define RELAND_CRT_NAME "none"
#endif

// Detect compiler features
#if RELAND_COMPILER_MSVC || RELAND_COMPILER_CLANG
#   undef  RELAND_HAVE_VECTORCALL
#   define RELAND_HAVE_VECTORCALL 1
#endif

// Detect CPU properties

#if RELAND_CPU_ARM_64 || RELAND_CPU_X86_64 || RELAND_CPU_X86_32
#   undef  RELAND_UNALIGNED_ACCESS
#   define RELAND_UNALIGNED_ACCESS 1
#endif

#if defined(__FMA__)
#   undef  RELAND_HW_FMA3
#   define RELAND_HW_FMA3 1
#endif

#if defined(__AVX__)
#   undef  RELAND_HW_AVX
#   define RELAND_HW_AVX 1
#endif

#if defined(__AVX2__)
#   undef  RELAND_HW_AVX2
#   define RELAND_HW_AVX2 1
#endif

#if defined(__SSE__) || defined(_M_X64) || (_M_IX86_FP >= 1)
#   undef  RELAND_HW_SSE
#   define RELAND_HW_SSE 1
#endif

#if defined(__SSE2__) || defined(_M_X64) || (_M_IX86_FP >= 2)
#   undef  RELAND_HW_SSE2
#   define RELAND_HW_SSE2 1
#endif

#if defined(__F16C__) || defined(__AVX2__)
#   undef  RELAND_HW_F16C
#   define RELAND_HW_F16C 1
#endif

#if defined(__ARM_NEON__) || defined(__aarch64__) || defined(_M_ARM) || defined(_M_ARM64)
#   undef  RELAND_HW_NEON
#   define RELAND_HW_NEON 1
#endif


#if defined(__VECTOR4DOUBLE__)
#   undef  RELAND_HW_QPX
#   define RELAND_HW_QPX 1
#endif

#if defined(__ALTIVEC__) || defined(__VEC__)
#   undef  RELAND_HW_VMX
#   define RELAND_HW_VMX 1
#endif

#if defined(__VSX__)
#   undef  RELAND_HW_VSX
#   define RELAND_HW_VSX 1
#endif


// Asserts configuration
#if !defined(RELAND_CONFIG_DO_ASSERT)
#   define RELAND_CONFIG_DO_ASSERT 1
#endif

#if !defined(RELAND_CONFIG_DO_VERIFY)
#   define RELAND_CONFIG_DO_VERIFY 1
#endif

#if !defined(RELAND_CONFIG_DO_ENSURE)
#   define RELAND_CONFIG_DO_ENSURE 1
#endif

// Configure build types
#if !defined(RELAND_BUILD_TYPE_DEVELOP)
#define RELAND_BUILD_TYPE_DEVELOP 1
#endif
#if !defined(RELAND_BUILD_TYPE_QA)
#define RELAND_BUILD_TYPE_QA 1
#endif
#if !defined(RELAND_BUILD_TYPE_RETAIL)
#define RELAND_BUILD_TYPE_RETAIL 1
#endif

// platform specific headers
#if RELAND_PLATFORM_WINDOWS
#   include <reland/headers.windows.hxx>
#elif RELAND_PLATFORM_LINUX
#   include <reland/headers.linux.hxx>
#elif RELAND_PLATFORM_ANDROID
#   include <reland/headers.android.hxx>
#else
#   error "Unsupported platform"
#endif

// disable compiler warnings
#if RELAND_COMPILER_MSVC
#   include <reland/compiler.msvc.warnings.hxx>
#elif RELAND_COMPILER_CLANG
#   include <reland/compiler.clang.warnings.hxx>
#elif RELAND_COMPILER_GCC
#   include <reland/compiler.gcc.warnings.hxx>
#endif
