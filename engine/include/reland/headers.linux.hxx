#pragma once

//
// POSIX
//
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <utime.h>
#include <ucontext.h>
#include <semaphore.h>
#include <sched.h>
#include <pthread.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <fcntl.h>
#include <errno.h>
#include <endian.h>
#include <arpa/inet.h>
#include <sys/mman.h>
#include <sys/user.h>

//
// NEON Support for ARM.
//
#if defined(__aarch64__) || defined(__arm__)

#include <arm_neon.h> // For all ARMS but not for TEGRA/ATOM

#elif defined(__x86_64__) || defined(__i386__)

#include <immintrin.h>
#include <mmintrin.h>
#include <smmintrin.h>
#include <x86intrin.h>
#include <xmmintrin.h>

#endif

//
// STL Headers.
//
#include <algorithm>
#include <array>
#include <limits>
#include <list>
#include <locale>
#include <map>
#include <memory>
#include <new>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <vector>
#include <string_view>
#include <atomic>

//
// C Library Headers.
//
#include <cassert>
#include <cctype>
#include <cerrno>
#include <cinttypes>
#include <climits>
#include <clocale>
#include <cmath>
#include <cstdarg>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
