#include <reland/module.core.hxx>
#include <fmt/format.h>

CORE_API void nonunique_function() {
    std::puts(fmt::format("Hello from {0:d} also known as {0:x}", (uintptr_t)&nonunique_function).c_str());
    std::puts("windows");
}
