#include <reland/module.core.hxx>
#include <reland/core/string.hxx>
#include <reland/core/diagnostic.hxx>

namespace reland
{
    CORE_API int string_format(char* buffer, size_t count, const char* format, va_list arglist) noexcept
    {
        va_list argcopy;
        va_copy(argcopy, arglist);
        auto result = RELAND_STRING_FORMAT_V(buffer, count, format, argcopy);
        va_end(argcopy);
        return result;
    }

    CORE_API std::string string_format_args(const char* format, va_list arglist) noexcept
    {
        //
        // Create output string. This uses NRVO.
        //
        std::string result{};

        //
        // Compute number of characters required to format string.
        //
        va_list args_to_check;
        va_copy(args_to_check, arglist);
#if defined(_MSC_VER)
        const int count = ::_vscprintf(format, args_to_check);
#else
        char tmpbuf[1];
        const int count = ::vsnprintf(tmpbuf, 0, format, args_to_check);
#endif
        va_end(args_to_check);

        //
        // Check whether string format is possible
        //
        if (count >= 0)
        {
            //
            // Allocate space in string.
            //
            result.resize(static_cast<size_t>(count));

            //
            // Do actual string format.
            //
            va_list args_to_format;
            va_copy(args_to_format, arglist);

#if defined(_MSC_VER)
            const int processed = ::_vsnprintf_s(
                &result[0],
                result.length() + 1,
                result.length(),
                format,
                args_to_format
            );
#else
            const int processed = ::vsnprintf(
                &result[0],
                result.length() + 1,
                format,
                args_to_format
            );
#endif
            va_end(args_to_format);

            //
            // Check if format was successful.
            //
            if (processed < 0)
            {
                result.clear();
            }
        }

        return result;
    }

    CORE_API std::string string_format(const char* format, ...) noexcept
    {
        //
        // Create output string. This uses NRVO.
        //
        std::string result{};

        //
        // Compute number of characters required to format string.
        //
        va_list args_to_check;
        va_start(args_to_check, format);
#if defined(_MSC_VER)
        const int count = ::_vscprintf(format, args_to_check);
#else
        char tmpbuf[1];
        const int count = ::vsnprintf(tmpbuf, 0, format, args_to_check);
#endif
        va_end(args_to_check);

        //
        // Check whether string format is possible
        //
        if (count >= 0)
        {
            //
            // Allocate space in string.
            //
            result.resize(static_cast<size_t>(count));

            //
            // Do actual string format.
            //
            va_list args_to_format;
            va_start(args_to_format, format);

#if defined(_MSC_VER)
            const int processed = ::_vsnprintf_s(
                &result[0],
                result.length() + 1,
                result.length(),
                format,
                args_to_format
            );
#else
            const int processed = ::vsnprintf(
                &result[0],
                result.length() + 1,
                format,
                args_to_format
            );
#endif
            va_end(args_to_format);

            //
            // Check if format was successful.
            //
            if (processed < 0)
            {
                result.clear();
            }
        }

        return result;
    }

#if RELAND_PLATFORM_WINDOWS
    CORE_API std::wstring string_format_args(const wchar_t* format, va_list arglist) noexcept
    {
        //
        // Create output string. This uses NRVO.
        //
        std::wstring result{};

        //
        // Compute number of characters required to format string.
        //
        va_list args_to_check;
        va_copy(args_to_check, arglist);
#if defined(_MSC_VER)
        const int count = ::_vscwprintf(format, args_to_check);
#else
        wchar_t tmpbuf[1];
        const int count = ::vsnwprintf(tmpbuf, 0, format, args_to_check);
#endif
        va_end(args_to_check);

        //
        // Check whether string format is possible
        //
        if (count >= 0)
        {
            //
            // Allocate space in string.
            //
            result.resize(static_cast<size_t>(count));

            //
            // Do actual string format.
            //
            va_list args_to_format;
            va_copy(args_to_format, arglist);

#if defined(_MSC_VER)
            const int processed = ::_vsnwprintf_s(
                &result[0],
                result.length() + 1,
                result.length(),
                format,
                args_to_format
            );
#else
            const int processed = ::vsnwprintf(
                &result[0],
                result.length() + 1,
                format,
                args_to_format
            );
#endif
            va_end(args_to_format);

            //
            // Check if format was successful.
            //
            if (processed < 0)
            {
                result.clear();
            }
        }

        return result;
    }

    CORE_API std::wstring string_format(const wchar_t* format, ...) noexcept
    {
        //
        // Create output string. This uses NRVO.
        //
        std::wstring result{};

        //
        // Compute number of characters required to format string.
        //
        va_list args_to_check;
        va_start(args_to_check, format);
#if defined(_MSC_VER)
        const int count = ::_vscwprintf(format, args_to_check);
#else
        wchar_t tmpbuf[1];
        const int count = ::vsnwprintf(tmpbuf, 0, format, args_to_check);
#endif
        va_end(args_to_check);

        //
        // Check whether string format is possible
        //
        if (count >= 0)
        {
            //
            // Allocate space in string.
            //
            result.resize(static_cast<size_t>(count));

            //
            // Do actual string format.
            //
            va_list args_to_format;
            va_start(args_to_format, format);

#if defined(_MSC_VER)
            const int processed = ::_vsnwprintf_s(
                &result[0],
                result.length() + 1,
                result.length(),
                format,
                args_to_format
            );
#else
            const int processed = ::vsnwprintf(
                &result[0],
                result.length() + 1,
                format,
                args_to_format
            );
#endif
            va_end(args_to_format);

            //
            // Check if format was successful.
            //
            if (processed < 0)
            {
                result.clear();
            }
        }

        return result;
    }
#endif

    CORE_API int string_scan_args(const char* value, const char* format, va_list arglist) noexcept
    {
        va_list args;
        va_copy(args, arglist);
#ifdef _MSC_VER
        const auto result = vsscanf_s(value, format, args);
#else
        const auto result = std::vsscanf(value, format, args);
#endif
        va_end(args);
        return result;
    }

    CORE_API int string_scan(const char* value, const char* format, ...) noexcept
    {
        va_list args;
        va_start(args, format);
#ifdef _MSC_VER
        const auto result = vsscanf_s(value, format, args);
#else
        const auto result = std::vsscanf(value, format, args);
#endif
        va_end(args);
        return result;
    }
}

namespace reland
{
    CORE_API void replace_all(std::string& string, std::string_view from, std::string_view to) noexcept
    {
        if (!string.empty())
        {
            size_t start_pos = 0;
            while ((start_pos = string.find(from, start_pos)) != std::string::npos)
            {
                string.replace(start_pos, from.length(), to);
                start_pos += to.length();
            }
        }
    }

    CORE_API std::vector<std::string_view> split_string(std::string_view value, std::string_view separator, bool remove_empty) noexcept
    {
        std::vector<std::string_view> result{};

        std::string_view::size_type start = 0;

        auto pos = value.find_first_of(separator, start);
        while (pos != std::string_view::npos)
        {
            if (pos != start || !remove_empty)
            {
                result.push_back(value.substr(start, pos - start));
            }

            start = pos + 1;
            pos = value.find_first_of(separator, start);
        }

        // `<=` is used check whether empty element is added too.
        if (start < value.length() || !remove_empty)
        {
            result.push_back(value.substr(start, value.length() - start));
        }

        return result;
    }

    CORE_API std::vector<std::string_view> split_string(std::string_view value, char separator, bool remove_empty) noexcept
    {
        std::vector<std::string_view> result{};

        std::string_view::size_type start = 0;

        auto pos = value.find_first_of(separator, start);
        while (pos != std::string_view::npos)
        {
            if (pos != start || !remove_empty)
            {
                result.push_back(value.substr(start, pos - start));
            }

            start = pos + 1;
            pos = value.find_first_of(separator, start);
        }

        // `<=` is used check whether empty element is added too.
        if (start < value.length() || !remove_empty)
        {
            result.push_back(value.substr(start, value.length() - start));
        }

        return result;
    }
}
