#include <reland/module.core.hxx>
#include <reland/core/uuid.hxx>
#include <reland/core/string.hxx>

namespace reland
{
    namespace
    {
        constexpr inline uint32_t from_hex_digit(char value) noexcept
        {
            uint32_t result = 0;

            if (value >= '0' && value <= '9')
            {
                result = static_cast<uint32_t>(value - '0');
            }
            else if (value >= 'a' && value <= 'f')
            {
                result = static_cast<uint32_t>(value + 10 - 'a');
            }
            else if (value >= 'A' && value <= 'F')
            {
                result = static_cast<uint32_t>(value + 10 - 'A');
            }

            return result;
        }

        uintmax_t parse_hex_number(const char* string, size_t count) noexcept
        {
            uintmax_t result = 0;

            for (; *string != 0 && count != 0; --count, ++string)
            {
                result *= 16;
                result += from_hex_digit(*string);
            }

            return result;
        }
    }

    int uuid::compare(uuid left, uuid right) noexcept
    {
        if (left.a != right.a)
        {
            return (left.a < right.a) ? -1 : 1;
        }

        if (left.b != right.b)
        {
            return (left.b < right.b) ? -1 : 1;
        }

        if (left.c != right.c)
        {
            return (left.c < right.c) ? -1 : 1;
        }

        if (left.d != right.d)
        {
            return (left.d < right.d) ? -1 : 1;
        }

        return 0;
    }

    CORE_API uuid make_uuid() noexcept
    {
        return {};
    }

    CORE_API bool to_string(std::string& out_result, uuid value, uuid_format format) noexcept
    {
        switch (format)
        {
        case uuid_format::digits:
            {
                out_result = string_format(
                    "%08" PRIx32 "%08" PRIx32 "%08" PRIx32 "%08" PRIx32,
                    value.a,
                    value.b,
                    value.c,
                    value.d
                );
                break;
            }
        case uuid_format::separated_digits:
            {
                out_result = string_format(
                    "%08" PRIx32 "-%04" PRIx32 "-%04" PRIx32 "-%04" PRIx32 "-%04" PRIx32 "%08" PRIx32,
                    value.a,
                    static_cast<uint16_t>(value.b >> 16),
                    static_cast<uint16_t>(value.b),
                    static_cast<uint16_t>(value.c >> 16),
                    static_cast<uint16_t>(value.c),
                    value.d
                );
                break;
            }
        default:
            {
                out_result.clear();
                break;
            }
        }

        return out_result.empty() != false;
    }

    CORE_API bool from_string(uuid& out_result, std::string_view value, uuid_format format) noexcept
    {
        const auto length = value.length();
        std::array<char, 32> buffer{};

        switch (format)
        {
        case uuid_format::digits:
            {
                if (length != 32)
                {
                    return false;
                }

                std::memcpy(&buffer[0], &value[0], buffer.size());
                break;
            }
        case uuid_format::separated_digits:
            {
                if (length != 36)
                {
                    return false;
                }

                const auto condition = (
                    (value[8] != '-') ||
                    (value[13] != '-') ||
                    (value[18] != '-') ||
                    (value[23] != '-')
                    );

                if (condition)
                {
                    return false;
                }

                std::memcpy(&buffer[0], &value[0], 8);
                std::memcpy(&buffer[8], &value[9], 4);
                std::memcpy(&buffer[12], &value[14], 4);
                std::memcpy(&buffer[16], &value[19], 4);
                std::memcpy(&buffer[20], &value[24], 12);

                break;
            }
        default:
            {
                RX_ASSERTF(false, "Unknown format type: %u", static_cast<unsigned int>(format));
                return false;
            }
        }

        out_result = uuid
        {
            static_cast<uint32_t>(parse_hex_number(&buffer[0], static_cast<size_t>(8))),
            static_cast<uint32_t>(parse_hex_number(&buffer[8], static_cast<size_t>(8))),
            static_cast<uint32_t>(parse_hex_number(&buffer[16], static_cast<size_t>(8))),
            static_cast<uint32_t>(parse_hex_number(&buffer[24], static_cast<size_t>(8)))
        };

        return true;
    }
}
