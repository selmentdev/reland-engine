#pragma once
#include <reland/detect.hxx>

#if RELAND_STATIC_BUILD
#define CORE_API
#else
#if defined(module_core_EXPORTS)
#define CORE_API    __cxx_lib_export
#else
#define CORE_API    __cxx_lib_import
#endif
#endif
