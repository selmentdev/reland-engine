#pragma once
#include <reland/module.core.hxx>

#if defined(_MSC_VER)

#define RELAND_STRING_FORMAT_F(buffer, size, format, ...)           ::_snprintf_s(buffer, size, _TRUNCATE, format, ## __VA_ARGS__)
#define RELAND_STRING_FORMAT_V(buffer, size, format, arglist)       ::vsnprintf_s(buffer, size, _TRUNCATE, format, arglist)

#else

#define RELAND_STRING_FORMAT_F(buffer, size, format, ...)           ::snprintf(buffer, size, format, ## __VA_ARGS__)
#define RELAND_STRING_FORMAT_V(buffer, size, format, arglist)       ::vsnprintf(buffer, size, format, arglist)

#endif

namespace reland
{
    template <typename... Args>
    __forceinline int string_format_raw(char* buffer, size_t buffer_size, const char* format, Args... args) noexcept
    {
#if defined(_MSC_VER)
        return ::_snprintf_s(buffer, buffer_size, _TRUNCATE, format, args...);
#else
        return ::snprintf(buffer, buffer_size, format, args...);
#endif
    }

    template <size_t Size, typename... Args>
    __forceinline int string_format_raw(std::array<char, Size>& buffer, const char* format, Args... args) noexcept
    {
#if defined(_MSC_VER)
        return ::_snprintf_s(buffer.data(), buffer.size(), _TRUNCATE, format, args...);
#else
        return ::snprintf(buffer.data(), buffer.size(), format, args...);
#endif
    }

    template <typename... Args>
    __forceinline int string_scan_raw(const char* string, const char* format, Args... args) noexcept
    {
#if defined(_MSC_VER)
        return ::sscanf_s(string, format, args...);
#else
        return sscanf(string, format, args...);
#endif
    }

#if RELAND_PLATFORM_WINDOWS
    template <typename... Args>
    __forceinline int string_format_raw(wchar_t* buffer, size_t buffer_size, const wchar_t* format, Args... args) noexcept
    {
#if defined(_MSC_VER)
        return ::_snwprintf_s(buffer, buffer_size, _TRUNCATE, format, args...);
#else
        return ::snwprintf(buffer, buffer_size, format, args...);
#endif
    }

    template <size_t Size, typename... Args>
    __forceinline int string_format_raw(std::array<wchar_t, Size>& buffer, const wchar_t* format, Args... args) noexcept
    {
#if defined(_MSC_VER)
        return ::_snwprintf_s(buffer.data(), buffer.size(), _TRUNCATE, format, args...);
#else
        return ::snwprintf(buffer.data(), buffer.size(), format, args...);
#endif
    }

    template <typename... Args>
    __forceinline int string_scan_raw(const wchar_t* string, const wchar_t* format, Args... args) noexcept
    {
#if defined(_MSC_VER)
        return ::swscanf_s(string, format, args...);
#else
        return swscanf(string, format, args...);
#endif
    }
#endif

    template <size_t Size>
    __forceinline void string_copy(std::array<char, Size>& output, std::string_view input) noexcept
    {
#if defined(_MSC_VER)
        strncpy_s(output.data(), output.size(), input.data(), input.length());
#else
        std::strncpy(output.data(), input.data(), (std::min)(output.size(), input.length()));
#endif
    }

    template <typename InputIterator, typename OutputIterator>
    __forceinline void safe_copy(OutputIterator output_first, OutputIterator output_last, InputIterator input_first, InputIterator input_last) noexcept
    {
        while (output_first != output_last && input_first != input_last)
        {
            (*output_first) = (*input_first);
            ++output_first;
            ++input_first;
        }
    }
}

namespace reland
{
    CORE_API int string_format(char* buffer, size_t count, const char* format, va_list arglist) noexcept;
    CORE_API std::string string_format_args(const char* format, va_list arglist) noexcept;
    CORE_API std::string string_format(const char* format, ...) noexcept;
    CORE_API std::wstring string_format_args(const wchar_t* format, va_list arglist) noexcept;
    CORE_API std::wstring string_format(const wchar_t* format, ...) noexcept;

    CORE_API int string_scan_args(const char* source, const char* format, va_list arglist) noexcept;
    CORE_API int string_scan(const char* value, const char* format, ...) noexcept;

    CORE_API void replace_all(std::string& string, std::string_view from, std::string_view to) noexcept;

    CORE_API std::vector<std::string_view> split_string(std::string_view value, std::string_view separator, bool remove_empty = true) noexcept;
    CORE_API std::vector<std::string_view> split_string(std::string_view value, char separator, bool remove_empty = true) noexcept;

    inline bool starts_with(std::string_view string, std::string_view prefix) noexcept
    {
        return string.length() >= prefix.length() && string.compare(0, prefix.length(), prefix) == 0;
    }

    inline bool ends_with(std::string_view string, std::string_view suffix) noexcept
    {
        return string.length() >= suffix.length() && string.compare(string.length() - suffix.length(), suffix.length(), suffix) == 0;
    }


    template <typename Container>
    inline std::string string_join(Container container, std::string_view separator) noexcept
    {
        std::string result{};

        bool first = true;

        for (auto&& element : container)
        {
            if (first)
            {
                first = false;
            }
            else
            {
                result += separator;
            }

            result += element;
        }

        return result;
    }

    inline std::string string_join(std::initializer_list<std::string_view> container, std::string_view separator) noexcept
    {
        std::string result{};

        bool first = true;

        for (auto&& element : container)
        {
            if (first)
            {
                first = false;
            }
            else
            {
                result += separator;
            }

            result += element;
        }

        return result;
    }

    inline std::string_view trim_left(std::string_view value) noexcept
    {
        auto offset = value.find_first_not_of(" \t\f\r\n\v\b");
        if (offset != std::string_view::npos)
        {
            return value.substr(offset);
        }

        return {};
    }

    inline std::string_view trim_right(std::string_view value) noexcept
    {
        auto offset = value.find_last_not_of(" \t\f\r\n\v\b");
        if (offset != std::string_view::npos)
        {
            return value.substr(0, offset + 1);
        }

        return value;
    }

    inline std::string_view trim(std::string_view value) noexcept
    {
        value = trim_left(value);
        value = trim_right(value);
        return value;
    }
}

namespace reland
{
    template <size_t Size>
    __forceinline bool to_string(std::array<char, Size>& buffer, float value) noexcept
    {
        return string_format_raw(buffer.data(), buffer.size(), "%f", static_cast<double>(value)) >= 0;
    }

    template <size_t Size>
    __forceinline bool to_string(std::array<char, Size>& buffer, double value) noexcept
    {
        return string_format_raw(buffer.data(), buffer.size(), "%f", value) >= 0;
    }

    template <size_t Size>
    __forceinline bool to_string(std::array<char, Size>& buffer, int value, bool hex = false) noexcept
    {
        return string_format_raw(buffer.data(), buffer.size(), hex ? "%x" : "%d", value) >= 0;
    }

    template <size_t Size>
    __forceinline bool to_string(std::array<char, Size>& buffer, long value, bool hex = false) noexcept
    {
        return string_format_raw(buffer.data(), buffer.size(), hex ? "%lx" : "%ld", value) >= 0;
    }

    template <size_t Size>
    __forceinline bool to_string(std::array<char, Size>& buffer, long long value, bool hex = false) noexcept
    {
        return string_format_raw(buffer.data(), buffer.size(), hex ? "%llx" : "%lld", value) >= 0;
    }

    template <size_t Size>
    __forceinline bool to_string(std::array<char, Size>& buffer, unsigned int value, bool hex = false) noexcept
    {
        return string_format_raw(buffer.data(), buffer.size(), hex ? "%x" : "%u", value) >= 0;
    }

    template <size_t Size>
    __forceinline bool to_string(std::array<char, Size>& buffer, unsigned long value, bool hex = false) noexcept
    {
        return string_format_raw(buffer.data(), buffer.size(), hex ? "%lx" : "%lu", value) >= 0;
    }

    template <size_t Size>
    __forceinline bool to_string(std::array<char, Size>& buffer, unsigned long long value, bool hex = false) noexcept
    {
        return string_format_raw(buffer.data(), buffer.size(), hex ? "%llx" : "%llu", value) >= 0;
    }
}

namespace reland
{
    __forceinline std::string_view to_string(bool value) noexcept
    {
        return value ? std::string_view{ "true" } : std::string_view{ "false" };
    }

    __forceinline std::string to_string(float value) noexcept
    {
        std::array<char, 32> buffer{};
        to_string(buffer, value);
        return { buffer.data() };
    }

    __forceinline std::string to_string(double value) noexcept
    {
        std::array<char, 32> buffer{};
        to_string(buffer, value);
        return { buffer.data() };
    }

    __forceinline std::string to_string(int value) noexcept
    {
        std::array<char, 48> buffer{};
        to_string(buffer, value);
        return { buffer.data() };
    }

    __forceinline std::string to_string(long value) noexcept
    {
        std::array<char, 80> buffer{};
        to_string(buffer, value);
        return { buffer.data() };
    }

    __forceinline std::string to_string(long long value) noexcept
    {
        std::array<char, 80> buffer{};
        to_string(buffer, value);
        return { buffer.data() };
    }

    __forceinline std::string to_string(unsigned int value) noexcept
    {
        std::array<char, 48> buffer{};
        to_string(buffer, value);
        return { buffer.data() };
    }

    __forceinline std::string to_string(unsigned long value) noexcept
    {
        std::array<char, 80> buffer{};
        to_string(buffer, value);
        return { buffer.data() };
    }

    __forceinline std::string to_string(unsigned long long value) noexcept
    {
        std::array<char, 80> buffer{};
        to_string(buffer, value);
        return { buffer.data() };
    }
}

namespace reland
{
    __forceinline size_t from_string(bool& value, const char* string) noexcept
    {
        if (std::string_view{ "true" } == string)
        {
            value = true;
            return 4;
        }
        else if (std::string_view{ "yes" } == string)
        {
            value = true;
            return 3;
        }
        else if (std::string_view{ "false" } == string)
        {
            value = false;
            return 5;
        }
        else if (std::string_view{ "no" } == string)
        {
            value = false;
            return 2;
        }

        value = false;
        return 0;
    }

    __forceinline size_t from_string(float& value, const char* string) noexcept
    {
        char* end = nullptr;
        value = strtof(string, &end);
        return static_cast<size_t>(end - string);
    }

    __forceinline size_t from_string(double& value, const char* string) noexcept
    {
        char* end = nullptr;
        value = strtod(string, &end);
        return static_cast<size_t>(end - string);
    }

    __forceinline size_t from_string(int& value, const char* string, int base = 10) noexcept
    {
        char* end = nullptr;
        value = static_cast<signed int>(strtol(string, &end, base));
        return static_cast<size_t>(end - string);
    }

    __forceinline size_t from_string(long& value, const char* string, int base = 10) noexcept
    {
        char* end = nullptr;
        value = strtol(string, &end, base);
        return static_cast<size_t>(end - string);
    }

    __forceinline size_t from_string(long long& value, const char* string, int base = 10) noexcept
    {
        char* end = nullptr;
        value = strtoll(string, &end, base);
        return static_cast<size_t>(end - string);
    }

    __forceinline size_t from_string(unsigned int& value, const char* string, int base = 10) noexcept
    {
        char* end = nullptr;
        value = static_cast<unsigned int>(strtoul(string, &end, base));
        return static_cast<size_t>(end - string);
    }

    __forceinline size_t from_string(unsigned long& value, const char* string, int base = 10) noexcept
    {
        char* end = nullptr;
        value = strtoul(string, &end, base);
        return static_cast<size_t>(end - string);
    }

    __forceinline size_t from_string(unsigned long long& value, const char* string, int base = 10) noexcept
    {
        char* end = nullptr;
        value = strtoull(string, &end, base);
        return static_cast<size_t>(end - string);
    }
}

namespace reland
{
    __forceinline size_t from_string(float& value, const std::string& string) noexcept
    {
        return from_string(value, string.c_str());
    }

    __forceinline size_t from_string(double& value, const std::string& string) noexcept
    {
        return from_string(value, string.c_str());
    }

    __forceinline size_t from_string(int& value, const std::string& string, int base = 10) noexcept
    {
        return from_string(value, string.c_str(), base);
    }

    __forceinline size_t from_string(long& value, const std::string& string, int base = 10) noexcept
    {
        return from_string(value, string.c_str(), base);
    }

    __forceinline size_t from_string(long long& value, const std::string& string, int base = 10) noexcept
    {
        return from_string(value, string.c_str(), base);
    }

    __forceinline size_t from_string(unsigned int& value, const std::string& string, int base = 10) noexcept
    {
        return from_string(value, string.c_str(), base);
    }

    __forceinline size_t from_string(unsigned long& value, const std::string& string, int base = 10) noexcept
    {
        return from_string(value, string.c_str(), base);
    }

    __forceinline size_t from_string(unsigned long long& value, const std::string& string, int base = 10) noexcept
    {
        return from_string(value, string.c_str(), base);
    }
}
