#pragma once
#include <reland/module.core.hxx>

namespace reland
{
    struct flags final
    {
        template <typename EnumType>
        static constexpr EnumType set(EnumType value, EnumType mask) noexcept
        {
            return EnumType(
                std::underlying_type_t<EnumType>(value) | std::underlying_type_t<EnumType>(mask)
            );
        }

        template <typename EnumType>
        static constexpr EnumType clear(EnumType value, EnumType mask) noexcept
        {
            return EnumType{
                std::underlying_type_t<EnumType>(value) & ~std::underlying_type_t<EnumType>(mask)
            };
        }

        template <typename EnumType>
        static constexpr EnumType toggle(EnumType value, EnumType mask) noexcept
        {
            return EnumType(
                std::underlying_type_t<EnumType>(value) ^ std::underlying_type_t<EnumType>(mask)
            );
        }

        template <typename EnumType>
        static constexpr EnumType mask(EnumType value, EnumType mask) noexcept
        {
            return EnumType(
                std::underlying_type_t<EnumType>(value) & std::underlying_type_t<EnumType>(mask)
            );
        }

        template <typename EnumType>
        static constexpr EnumType change(EnumType value, EnumType set, EnumType clear) noexcept
        {
            return EnumType(
                (std::underlying_type_t<EnumType>(value) & std::underlying_type_t<EnumType>(clear)) | std::underlying_type_t<EnumType>(set)
            );
        }

        template <typename EnumType>
        static constexpr bool has(EnumType value, EnumType mask) noexcept
        {
            return (std::underlying_type_t<EnumType>(value) & std::underlying_type_t<EnumType>(mask)) == std::underlying_type_t<EnumType>(mask);
        }

        template <typename EnumType>
        static constexpr bool any(EnumType value, EnumType mask) noexcept
        {
            return (std::underlying_type_t<EnumType>(value) & std::underlying_type_t<EnumType>(mask)) != std::underlying_type_t<EnumType>(EnumType{});
        }

        template <typename EnumType>
        static constexpr bool none(EnumType value, EnumType mask) noexcept
        {
            return (std::underlying_type_t<EnumType>(value) & std::underlying_type_t<EnumType>(mask)) == std::underlying_type_t<EnumType>(EnumType{});
        }
    };
}
