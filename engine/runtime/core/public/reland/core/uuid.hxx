#pragma once
#include <reland/module.core.hxx>
#include <reland/core/diagnostic.hxx>

namespace reland
{
    enum struct uuid_format
    {
        digits,             //!< 08x08x08x08x
        separated_digits,   //!< 08x-04x-04x-04x-04x-08x
    };

    struct uuid final
    {
    public:
        uint32_t a;
        uint32_t b;
        uint32_t c;
        uint32_t d;

    public:
        uint32_t& operator[] (size_t index) noexcept;
        const uint32_t& operator[] (size_t index) const noexcept;
        bool is_empty() const noexcept;

        static int compare(uuid left, uuid right) noexcept;
    };

    static_assert(std::is_trivial_v<uuid>);
    static_assert(std::is_standard_layout_v<uuid>);

    inline uint32_t& uuid::operator[] (size_t index) noexcept
    {
        RX_ASSERT(index < 4);

        switch (index)
        {
        default:
        case 0:
            {
                return this->a;
            }
        case 1:
            {
                return this->b;
            }
        case 2:
            {
                return this->c;
            }
        case 3:
            {
                return this->d;
            }
        }
    }

    inline const uint32_t& uuid::operator[] (size_t index) const noexcept
    {
        RX_ASSERT(index < 4);

        switch (index)
        {
        default:
        case 0:
            {
                return this->a;
            }
        case 1:
            {
                return this->b;
            }
        case 2:
            {
                return this->c;
            }
        case 3:
            {
                return this->d;
            }
        }
    }

    inline bool uuid::is_empty() const noexcept
    {
        return this->a == 0
            && this->b == 0
            && this->c == 0
            && this->d == 0;
    }

    inline bool operator== (uuid left, uuid right) noexcept
    {
        return uuid::compare(left, right) == 0;
    }

    inline bool operator!= (uuid left, uuid right) noexcept
    {
        return uuid::compare(left, right) != 0;
    }

    inline bool operator<= (uuid left, uuid right) noexcept
    {
        return uuid::compare(left, right) <= 0;
    }

    inline bool operator>= (uuid left, uuid right) noexcept
    {
        return uuid::compare(left, right) >= 0;
    }

    inline bool operator< (uuid left, uuid right) noexcept
    {
        return uuid::compare(left, right) < 0;
    }

    inline bool operator> (uuid left, uuid right) noexcept
    {
        return uuid::compare(left, right) > 0;
    }

    CORE_API uuid make_uuid() noexcept;
    CORE_API bool to_string(std::string& out_result, uuid value, uuid_format format = uuid_format::digits) noexcept;
    CORE_API bool from_string(uuid& out_result, std::string_view value, uuid_format format = uuid_format::digits) noexcept;
}
