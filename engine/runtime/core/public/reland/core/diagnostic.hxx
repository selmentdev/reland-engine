#pragma once

#if RELAND_BUILD_TYPE_DEVELOP

#define RX_DEBUGF(format, ...) \
{ \
    ::reland::diagnostic::debug::writef(format, ## __VA_ARGS__); \
}

#else

#define RX_DEBUGF(format, ...)

#endif

#if !RELAND_CONFIG_DO_ASSERT

#define RX_ASSERT(expression) \
{ \
    if (!(expression)) \
    { \
        if (::Graphyte::Diagnostic::Debug::HandleAssertionFailure(#expression, __FILE__, __LINE__)) \
        { \
            __cxx_debugbreak(); \
        } \
    } \
}

#define RX_ASSERTF(expression, format, ...) \
{ \
    if (!(expression)) \
    { \
        if (::Graphyte::Diagnostic::Debug::HandleAssertionFailure(#expression, __FILE__, __LINE__, format, ## __VA_ARGS__)) \
        { \
            __cxx_debugbreak(); \
        } \
    } \
}

#else

#define RX_ASSERT(expression)                   { ((void)0); __cxx_assume(expression); }
#define RX_ASSERTF(expression, format, ...)     { ((void)0); __cxx_assume(expression); }

#endif

#if RELAND_CONFIG_DO_VERIFY

#define RX_VERIFY(expression) \
{ \
    if (!(expression)) \
    { \
        if (::Graphyte::Diagnostic::Debug::HandleAssertionFailure(#expression, __FILE__, __LINE__)) \
        { \
            __cxx_debugbreak(); \
        } \
    } \
}

#define RX_VERIFYF(expression, format, ...) \
{ \
    if (!(expression)) \
    { \
        if (::Graphyte::Diagnostic::Debug::HandleAssertionFailure(#expression, __FILE__, __LINE__, format, ## __VA_ARGS__)) \
        { \
            __cxx_debugbreak(); \
        } \
    } \
}

#else

#define RX_VERIFY(expression) \
{ \
    if (!(expression)) \
    { \
        __cxx_assume(expression); \
    } \
}

#define RX_VERIFYF(expression, format, ...) \
{ \
    if (!(expression)) \
    { \
        __cxx_assume(expression); \
    } \
}

#endif

#if RELAND_CONFIG_DO_ENSURE

#define RX_ENSURE(expression) \
( \
    (expression) || \
    (::Graphyte::Diagnostic::Debug::HandleAssertionFailure(#expression, __FILE__, __LINE__)) || \
    (__cxx_debugbreak(), false) \
)

#define RX_ENSUREF(expression, format, ...) \
( \
    (expression) || \
    (::Graphyte::Diagnostic::Debug::HandleAssertionFailure(#expression, __FILE__, __LINE__, format, ## __VA_ARGS__)) || \
    (__cxx_debugbreak(), false) \
)


#else

#define RX_ENSURE(expression)                      (!!(expression))
#define RX_ENSUREF(expression, format, ...)       (!!(expression))

#endif


#define RX_ASSERT_UNIMPLEMENTED()           RX_ASSERTF(false, "Unimplemented: %s", __FUNCTION__)

#if RELAND_CONFIG_DO_ASSERT

#define RX_ASSERT_SINGLE_CALL_MSG(message, ...) { \
        static bool __cxx_unique_name(gx_single_call_scope_mark) = false; \
        RX_ASSERTF(__cxx_unique_name(gx_single_call_scope_mark) == false, message, ## __VA_ARGS__); \
        (void)__cxx_unique_name(gx_single_call_scope_mark); \
        __cxx_unique_name(gx_single_call_scope_mark) = true; \
    }

#define RX_ASSERT_SINGLE_CALL()             RX_ASSERT_SINGLE_CALL_MSG(" called more than once")

#else

#define RX_ASSERT_SINGLE_CALL_MSG(message)
#define RX_ASSERT_SINGLE_CALL()

#endif


#define RX_INTERNAL_DECLARE_TRACE_SWITCH_EXTERN(switch_name, runtime_level, compile_level) \
    extern struct TraceSwitch##switch_name : public ::Graphyte::Diagnostic::TraceSwitch< \
        ::Graphyte::Diagnostic::TraceLevel::runtime_level, \
        ::Graphyte::Diagnostic::TraceLevel::compile_level> \
    { \
        __forceinline TraceSwitch##switch_name() noexcept \
            : TraceSwitch(#switch_name) \
        { \
        } \
    } switch_name

#define RX_INTERNAL_DECLARE_TRACE_SWITCH_STATIC(switch_name, runtime_level, compile_level) \
    static struct TraceSwitch##switch_name : public ::Graphyte::Diagnostic::TraceSwitch< \
        ::Graphyte::Diagnostic::TraceLevel::runtime_level, \
        ::Graphyte::Diagnostic::TraceLevel::compile_level> \
    { \
        __forceinline TraceSwitch##switch_name() noexcept \
            : TraceSwitch(#switch_name) \
        { \
        } \
    } switch_name

#define RX_DEFINE_TRACE_SWITCH(switch_name) \
    struct TraceSwitch##switch_name switch_name{}

#define RX_DECLARE_TRACE_SWITCH(switch_name, runtime_level, compile_level) \
    RX_INTERNAL_DECLARE_TRACE_SWITCH_EXTERN(switch_name, runtime_level, compile_level)

#define RX_DEFINE_TRACE_SWITCH_CLASS(type, switch_name) \
    type::TraceSwitch##switch_name type::switch_name{}

#define RX_DECLARE_TRACE_SWITCH_CLASS(switch_name, runtime_level, compile_level) \
    protected: RX_INTERNAL_DECLARE_TRACE_SWITCH_STATIC(switch_name, runtime_level, compile_level)


#define RX_GET_TRACE_SWITCH_LEVEL(switch_name) \
    switch_name.GetLevel()

#define RX_SET_TRACE_SWITCH_LEVEL(switch_name, level) \
    switch_name.SetLevel(::Graphyte::Diagnostic::TraceLevel::level)



#define RX_LOG(switch_name, level, format, ...) \
    { \
        if constexpr (::Graphyte::Diagnostic::TraceLevel::level == ::Graphyte::Diagnostic::TraceLevel::Fatal) \
        { \
            ::Graphyte::Diagnostic::Trace::Dispatch(::Graphyte::Diagnostic::TraceLevel::level, switch_name.GetName(), format, ## __VA_ARGS__); \
            if (::Graphyte::Diagnostic::Debug::IsDebuggerAttached()) \
            { \
                __cxx_debugbreak(); \
            } \
            ::Graphyte::Diagnostic::Debug::HandleAssertionFailure("", __FILE__, __LINE__, format, ## __VA_ARGS__); \
        } \
        else \
        { \
            if constexpr (::Graphyte::Diagnostic::TraceLevel::level <= static_cast<::Graphyte::Diagnostic::TraceLevel>(TraceSwitch##switch_name::CompileLevel)) \
            { \
                if (switch_name.CanDispatch(::Graphyte::Diagnostic::TraceLevel::level)) \
                { \
                    ::Graphyte::Diagnostic::Trace::Dispatch(::Graphyte::Diagnostic::TraceLevel::level, switch_name.GetName(), format, ## __VA_ARGS__); \
                } \
            } \
        } \
    }

#define RX_LOG_WHEN(condition, switch_name, level, format, ...) \
    { \
        if constexpr() \
        { \
            if (condition) \
            { \
                if (switch_name.CanDispatch(::Graphyte::Diagnostic::TraceLevel::level)) \
                { \
                    ::Graphyte::Diagnostic::Trace::Dispatch(::Graphyte::Diagnostic::TraceLevel::level, switch_name.GetName(), format, ## __VA_ARGS__); \
                } \
            } \
        } \
    }


namespace Graphyte::Diagnostic
{
    class RecursionWatcher final
    {
    private:
        size_t& m_Counter;

    public:
        RecursionWatcher() = delete;
        RecursionWatcher(const RecursionWatcher&) = delete;
        RecursionWatcher& operator= (const RecursionWatcher&) = delete;

    public:
        RecursionWatcher(size_t& counter) noexcept
            : m_Counter{ counter }
        {
            ++m_Counter;
        }

        ~RecursionWatcher() noexcept
        {
            --m_Counter;
        }
    };
}


#if RELAND_CONFIG_DO_ASSERT

#define RX_RECURSION() \
    static size_t __cxx_unique_name(gx_recursion_watch_count){}; \
    RX_ASSERTF(__cxx_unique_name(gx_recursion_watch_count) == 0, "Recursion detected"); \
    const ::Graphyte::Diagnostic::RecursionWatcher __cxx_unique_name(gx_recursion_watch){ __cxx_unique_name(gx_recursion_watch_count) }

#define RX_RECURSIONF(format, ...) \
    static size_t __cxx_unique_name(gx_recursion_watch_count){}; \
    RX_ASSERTF(__cxx_unique_name(gx_recursion_watch_count) == 0, format, __VA_ARGS__); \
    const ::Graphyte::Diagnostic::RecursionWatcher __cxx_unique_name(gx_recursion_watch){ __cxx_unique_name(gx_recursion_watch_count) }

#else

#define RX_RECURSION()      ((void)0)
#define RX_RECURSIONF()     ((void)0)

#endif
