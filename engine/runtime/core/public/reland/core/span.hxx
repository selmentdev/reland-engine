#pragma once
#include <reland/module.core.hxx>
#include <reland/core/diagnostic.hxx>

namespace reland
{
    template <typename T>
    class span final
    {
    public:
        using element_type = T;
        using value_type = std::remove_cv_t<T>;

        using difference_type = std::ptrdiff_t;
        using size_type = size_t;
        using index_type = std::ptrdiff_t;

        using pointer = T * ;
        using const_pointer = const T*;

        using reference = T & ;
        using const_reference = const T&;

        using iterator = T * ;
        using const_iterator = const T*;

        using reverse_iterator = std::reverse_iterator<iterator>;
        using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    private:
        pointer m_data;
        size_type m_size;

    public:
        constexpr span() noexcept
            : m_data{}
            , m_size{}
        {
        }

        constexpr span(pointer data, size_type length) noexcept
            : m_data{ data }
            , m_size{ length }
        {
        }

        template <size_type N>
        constexpr span(value_type(&array)[N]) noexcept
            : m_data{ array }
            , m_size{ N }
        {
        }

        constexpr span(const span& rhs) noexcept
            : m_data{ rhs.m_data }
            , m_size{ rhs.m_size }
        {
        }

        constexpr span& operator= (const span& rhs) noexcept
        {
            m_data = rhs.m_data;
            m_size = rhs.m_size;
            return (*this);
        }

        constexpr iterator begin() const noexcept
        {
            return m_data;
        }
        constexpr iterator end() const noexcept
        {
            return m_data + m_size;
        }

        constexpr const_iterator cbegin() const noexcept
        {
            return m_data;
        }
        constexpr const_iterator cend() const noexcept
        {
            return m_data + m_size;
        }

        constexpr reverse_iterator rbegin() const noexcept
        {
            return reverse_iterator(end());
        }
        constexpr reverse_iterator rend() const noexcept
        {
            return reverse_iterator(begin());
        }

        constexpr const_reverse_iterator crbegin() const noexcept
        {
            return const_reverse_iterator(cend());
        }
        constexpr const_reverse_iterator crend() const noexcept
        {
            return const_reverse_iterator(cbegin());
        }

        constexpr size_type size() const noexcept
        {
            return m_size;
        }
        constexpr size_type length() const noexcept
        {
            return m_size;
        }
        constexpr size_type max_length() const noexcept
        {
            return std::numeric_limits<size_type>::max();
        }
        constexpr size_type size_bytes() const noexcept
        {
            return m_size * sizeof(value_type);
        }

        constexpr bool empty() const noexcept
        {
            return m_size == 0;
        }

        constexpr pointer data() const noexcept
        {
            return m_data;
        }

        constexpr const_reference operator[](size_type position) const noexcept
        {
            RX_ASSERT(position < this->length());
            return m_data[position];
        }

        constexpr reference operator[] (size_type position) noexcept
        {
            RX_ASSERT(position < this->length());
            return m_data[position];
        }

        constexpr const_reference front() const noexcept
        {
            RX_ASSERT(!this->empty());
            return m_data[0];
        }

        constexpr reference front() noexcept
        {
            RX_ASSERT(!this->empty());
            return m_data[0];
        }

        constexpr const_reference back() const noexcept
        {
            RX_ASSERT(!this->empty());
            return m_data[m_size - 1];
        }

        constexpr reference back() noexcept
        {
            RX_ASSERT(!this->empty());
            return m_data[m_size - 1];
        }

        constexpr void clear() noexcept
        {
            m_data = nullptr;
            m_size = 0;
        }

        constexpr void remove_prefix(size_type count) noexcept
        {
            RX_ASSERT(count <= length());
            m_data += count;
            m_size -= count;
        }

        constexpr void remove_suffix(size_type count) noexcept
        {
            RX_ASSERT(count <= length());
            m_size -= count;
        }

        constexpr void swap(span& other) noexcept
        {
            std::swap(m_data, other.m_data);
            std::swap(m_size, other.m_size);
        }

        constexpr span first(difference_type count) const noexcept
        {
            RX_ASSERT(static_cast<size_type>(count) < m_size);
            return span{ m_data, static_cast<size_type>(count) };
        }

        template <difference_type Length>
        constexpr span first() const noexcept
        {
            RX_ASSERT(static_cast<size_type>(Length) < m_size);
            return span{ m_data, static_cast<size_type>(Length) };
        };

        constexpr span last(difference_type count) const noexcept
        {
            RX_ASSERT(static_cast<size_type>(count) < m_size);
            return span{ m_data + m_size - count, static_cast<size_type>(count) };
        }

        template <difference_type Length>
        constexpr span last() const noexcept
        {
            RX_ASSERT(static_cast<size_type>(Length) < m_size);
            return span{ m_data + m_size - Length, static_cast<size_type>(Length) };
        }

        template <difference_type Offset, difference_type Length>
        constexpr span subspan() const noexcept
        {
            RX_ASSERT(static_cast<size_type>(Offset + Length) <= m_size);
            return span{ m_data + Offset, static_cast<size_type>(Length) };
        }

        constexpr span subspan(difference_type offset, difference_type count) const noexcept
        {
            RX_ASSERT(static_cast<size_type>(offset + count) <= m_size);
            return span{ m_data + offset, static_cast<size_type>(count) };
        };
    };

    template <typename T>
    constexpr span<const std::byte> as_bytes(span<T> value) noexcept
    {
        return span<const std::byte>{ reinterpret_cast<const std::byte*>(value.data()), value.size_bytes() };
    };

    template <typename T, typename = std::enable_if_t<!std::is_const<T>::value>>
    constexpr span<std::byte> as_writable_bytes(span<T> value) noexcept
    {
        return span<std::byte>{ reinterpret_cast<std::byte*>(value.data()), value.size_bytes() };
    };
}
