#pragma once
#include <reland/module.core.hxx>
#include <random>

CORE_API void rx_initialize_library() noexcept;

namespace reland
{
    enum struct byte_encoding : uint32_t
    {
        little_endian = 0x01020304,
        big_endian = 0x04030201,
    };

    //
    // float bitcasting
    //

    constexpr float to_float(uint32_t value) noexcept
    {
        ieee754_float pun{};
        pun.as_uint = value;
        return pun.value;
    }

    constexpr uint32_t from_float(float value) noexcept
    {
        ieee754_float pun{};
        pun.value = value;
        return pun.as_uint;
    }

    constexpr double to_double(uint64_t value) noexcept
    {
        ieee754_double pun{};
        pun.as_uint = value;
        return pun.value;
    }

    constexpr uint64_t from_double(double value) noexcept
    {
        ieee754_double pun{};
        pun.value = value;
        return pun.as_uint;
    }


    //
    // Byte swapping
    //

    template <typename T>
    constexpr T byteswap(T value) noexcept = delete;

    template <>
    constexpr uint8_t byteswap<uint8_t>(uint8_t value) noexcept
    {
        return value;
    }

    template <>
    constexpr uint16_t byteswap<uint16_t>(uint16_t value) noexcept
    {
        return static_cast<uint16_t>(
            ((value & static_cast<uint16_t>(0x00ff)) << 8) |
            ((value & static_cast<uint16_t>(0xff00)) >> 8)
        );
    }

    template <>
    constexpr uint32_t byteswap<uint32_t>(uint32_t value) noexcept
    {
        return static_cast<uint32_t>(
            ((value & static_cast<uint32_t>(0x0000'00ff)) << 24) |
            ((value & static_cast<uint32_t>(0x0000'ff00)) <<  8) |
            ((value & static_cast<uint32_t>(0x00ff'0000)) >>  8) |
            ((value & static_cast<uint32_t>(0xff00'0000)) >> 24)
        );
    }

    template <>
    constexpr uint64_t byteswap<uint64_t>(uint64_t value) noexcept
    {
        return static_cast<uint64_t>(
            ((value & static_cast<uint64_t>(0x0000'0000'0000'00ff)) << 56) |
            ((value & static_cast<uint64_t>(0x0000'0000'0000'ff00)) << 40) |
            ((value & static_cast<uint64_t>(0x0000'0000'00ff'0000)) << 24) |
            ((value & static_cast<uint64_t>(0x0000'0000'ff00'0000)) <<  8) |
            ((value & static_cast<uint64_t>(0x0000'00ff'0000'0000)) >>  8) |
            ((value & static_cast<uint64_t>(0x0000'ff00'0000'0000)) >> 24) |
            ((value & static_cast<uint64_t>(0x00ff'0000'0000'0000)) >> 40) |
            ((value & static_cast<uint64_t>(0xff00'0000'0000'0000)) >> 56)
        );
    }

#if RELAND_HW_AVX
    template <>
    __forceinline __m128i byteswap<__m128i>(__m128i value) noexcept
    {
        return _mm_shuffle_epi8(value, _mm_set_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15));
    }
#endif


    //
    // Unaligned access.
    //

    template <typename T>
    __forceinline T from_unaligned(const void* source) noexcept
    {
        static_assert(std::is_integral_v<T> || std::is_floating_point_v<T>);
        return *reinterpret_cast<const T*>(source);
    }

    template <typename T>
    __forceinline void from_unaligned(T& value, const void* source) noexcept
    {
        value = from_unaligned<T>(source);
    }

    template <typename T>
    __forceinline void to_unaligned(void* destination, T value) noexcept
    {
        static_assert(std::is_integral_v<T> || std::is_floating_point_v<T>);
        *reinterpret_cast<T*>(destination) = value;
    }


    //
    // Network endian support
    //

    template <typename T>
    constexpr T network_to_host(T value) noexcept
    {
        static_assert(std::is_integral_v<T>);
#if RELAND_ENDIAN_LITTLE
        return byteswap<T>(value);
#else
        return value;
#endif
    }

    template <typename T>
    constexpr T host_to_network(T value) noexcept
    {
        static_assert(std::is_integral_v<T>);
#if RELAND_ENDIAN_LITTLE
        return byteswap<T>(value);
#else
        return value;
#endif
    }

    template <typename T>
    constexpr T to_big_endian(T value) noexcept
    {
        static_assert(std::is_integral_v<T>);
#if RELAND_ENDIAN_LITTLE
        return byteswap<T>(value);
#else
        return value;
#endif
    }

    template <typename T>
    constexpr T from_big_endian(T value) noexcept
    {
        static_assert(std::is_integral_v<T>);
#if RELAND_ENDIAN_LITTLE
        return byteswap<T>(value);
#else
        return value;
#endif
    }

    template <typename T>
    constexpr T to_little_endian(T value) noexcept
    {
        static_assert(std::is_integral_v<T>);
#if RELAND_ENDIAN_LITTLE
        return value;
#else
        return byteswap<T>(value);
#endif
    }

    template <typename T>
    constexpr T from_little_endian(T value) noexcept
    {
        static_assert(std::is_integral_v<T>);
#if RELAND_ENDIAN_LITTLE
        return value;
#else
        return byteswap<T>(value);
#endif
    }


    //
    // Load / store endian functions.
    //

    template <typename T>
    __forceinline T read_little_endian(const void* source) noexcept
    {
        return from_little_endian<T>(from_unaligned<T>(source));
    }

    template <typename T>
    __forceinline T read_big_endian(const void* source) noexcept
    {
        return from_big_endian<T>(from_unaligned<T>(source));
    }

    template <typename T>
    __forceinline void read_little_endian(T& value, const void* source) noexcept
    {
        value = to_little_endian<T>(from_unaligned<T>(source));
    }

    template <typename T>
    __forceinline void read_big_endian(T& value, const void* source) noexcept
    {
        value = to_big_endian<T>(from_unaligned<T>(source));
    }

    template <typename T>
    __forceinline void write_little_endian(void* destination, T value) noexcept
    {
        to_unaligned<T>(destination, to_little_endian<T>(value));
    }

    template <typename T>
    __forceinline void write_big_endian(void* destination, T value) noexcept
    {
        to_unaligned<T>(destination, to_big_endian<T>(value));
    }


    //
    // Endian functions for float types.
    //

    template <>
    __forceinline float read_little_endian<float>(const void* source) noexcept
    {
        return to_float(read_little_endian<uint32_t>(source));
    }

    template <>
    __forceinline float read_big_endian<float>(const void* source) noexcept
    {
        return to_float(read_big_endian<uint32_t>(source));
    }

    template <>
    __forceinline void read_little_endian<float>(float& value, const void* source) noexcept
    {
        value = to_float(read_little_endian<uint32_t>(source));
    }

    template <>
    __forceinline void read_big_endian<float>(float& value, const void* source) noexcept
    {
        value = to_float(read_big_endian<uint32_t>(source));
    }

    template <>
    __forceinline void write_little_endian<float>(void* destination, float value) noexcept
    {
        to_unaligned<uint32_t>(destination, to_little_endian<uint32_t>(from_float(value)));
    }

    template <>
    __forceinline void write_big_endian<float>(void* destination, float value) noexcept
    {
        to_unaligned<uint32_t>(destination, to_big_endian<uint32_t>(from_float(value)));
    }


    //
    // Endian functions for double types.
    //

    template <>
    __forceinline double read_little_endian<double>(const void* source) noexcept
    {
        return to_double(read_little_endian<uint64_t>(source));
    }

    template <>
    __forceinline double read_big_endian<double>(const void* source) noexcept
    {
        return to_double(read_big_endian<uint64_t>(source));
    }

    template <>
    __forceinline void read_little_endian<double>(double& value, const void* source) noexcept
    {
        value = to_double(read_little_endian<uint64_t>(source));
    }

    template <>
    __forceinline void read_big_endian<double>(double& value, const void* source) noexcept
    {
        value = to_double(read_big_endian<uint64_t>(source));
    }

    template <>
    __forceinline void write_little_endian<double>(void* destination, double value) noexcept
    {
        to_unaligned<uint64_t>(destination, to_little_endian<uint64_t>(from_double(value)));
    }

    template <>
    __forceinline void write_big_endian<double>(void* destination, double value) noexcept
    {
        to_unaligned<uint64_t>(destination, to_big_endian<uint64_t>(from_double(value)));
    }


    //
    // Alignment.
    //

    template <typename T>
    constexpr bool is_aligned(T value, T alignment) noexcept
    {
        static_assert(std::is_unsigned_v<T>);
        return (value & (alignment - 1)) == 0;
    }

    __forceinline bool is_aligned(const void* pointer, std::align_val_t alignment) noexcept
    {
        return is_aligned<uintptr_t>(reinterpret_cast<uintptr_t>(pointer), static_cast<uintptr_t>(alignment));
    }

    template <typename T>
    constexpr T align_up(T value, T alignment) noexcept
    {
        static_assert(std::is_unsigned_v<T>);
        return (value + (alignment - 1)) & (~(alignment - 1));
    }

    template <typename T>
    constexpr T align_down(T value, T alignment) noexcept
    {
        static_assert(std::is_unsigned_v<T>);
        return value & (~(alignment - 1));
    }

    __forceinline void* align_up(void* pointer, std::align_val_t alignment) noexcept
    {
        return reinterpret_cast<void*>(align_up<uintptr_t>(reinterpret_cast<uintptr_t>(pointer), static_cast<uintptr_t>(alignment)));
    }

    __forceinline void* align_down(void* pointer, std::align_val_t alignment) noexcept
    {
        return reinterpret_cast<void*>(align_down<uintptr_t>(reinterpret_cast<uintptr_t>(pointer), static_cast<uintptr_t>(alignment)));
    }

    template <typename T>
    constexpr bool ispow2(T value) noexcept
    {
        static_assert(std::is_unsigned_v<T>);
        return value > 0 &&  (value & (value - 1)) == 0;
    }

    template <typename T>
    constexpr T ceilp2(T value) noexcept
    {
        value = value - 1;
        value |= value >> 1;
        value |= value >> 2;
        value |= value >> 4;

        if constexpr (sizeof(T) > 1)
        {
            value |= value >> 8;

            if constexpr (sizeof(T) > 2)
            {
                value |= value >> 16;

                if constexpr (sizeof(T) > 4)
                {
                    value |= value >> 32;
                }
            }
        }

        return value + 1;
    }

    template <typename T>
    constexpr T floorp2(T value) noexcept
    {
        value |= value >> 1;
        value |= value >> 2;
        value |= value >> 4;

        if constexpr (sizeof(T) > 1)
        {
            value |= value >> 8;

            if constexpr (sizeof(T) > 2)
            {
                value |= value >> 16;

                if constexpr (sizeof(T) > 4)
                {
                    value |= value >> 32;
                }
            }
        }

        return value - (value >> 1);
    }

    template <typename T>
    __forceinline T* advance_pointer(T* value, ptrdiff_t offset) noexcept
    {
        return reinterpret_cast<T*>(reinterpret_cast<std::byte*>(value) + offset);
    }

    template <typename T>
    __forceinline const T* AdvancePointer(const T* value, ptrdiff_t offset) noexcept
    {
        return reinterpret_cast<const T*>(reinterpret_cast<const std::byte*>(value) + offset);
    }


    //
    // Integer parts access.
    //

    template <typename T, typename S>
    constexpr T high_part(S value) noexcept = delete;

    template <>
    constexpr uint8_t high_part<uint8_t, uint16_t>(uint16_t value) noexcept
    {
        return static_cast<uint8_t>(value >> 8);
    }

    template <>
    constexpr uint16_t high_part<uint16_t, uint32_t>(uint32_t value) noexcept
    {
        return static_cast<uint16_t>(value >> 16);
    }

    template <>
    constexpr uint32_t high_part<uint32_t, uint64_t>(uint64_t value) noexcept
    {
        return static_cast<uint32_t>(value >> 32);
    }

    template <typename T, typename S>
    constexpr T low_part(S value) noexcept = delete;

    template <>
    constexpr uint8_t low_part<uint8_t, uint16_t>(uint16_t value) noexcept
    {
        return static_cast<uint8_t>(value);
    }

    template <>
    constexpr uint16_t low_part<uint16_t, uint32_t>(uint32_t value) noexcept
    {
        return static_cast<uint16_t>(value);
    }

    template <>
    constexpr uint32_t low_part<uint32_t, uint64_t>(uint64_t value) noexcept
    {
        return static_cast<uint32_t>(value);
    }


    //
    // Bit functions.
    //

    template <typename T>
    constexpr T rotl(T value, int bits) noexcept
    {
        static_assert(std::is_integral_v<T>);
        using UnsignedT = std::make_unsigned_t<T>;

        return static_cast<T>((static_cast<UnsignedT>(value) << bits) | (static_cast<UnsignedT>(value) >> ((CHAR_BIT * sizeof(T)) - static_cast<unsigned>(bits))));
    }

    template <typename T>
    constexpr T rotr(T value, int bits) noexcept
    {
        static_assert(std::is_integral_v<T>);
        using UnsignedT = std::make_unsigned_t<T>;
        return static_cast<T>((static_cast<UnsignedT>(value) >> bits) | (static_cast<UnsignedT>(value) << ((CHAR_BIT * sizeof(T)) - static_cast<unsigned>(bits))));
    }

    template <typename T>
    constexpr T shll(T value, int bits) noexcept
    {
        static_assert(std::is_integral_v<T>);
        return static_cast<T>(static_cast<std::make_unsigned_t<T>>(value) << bits);
    }

    template <typename T>
    constexpr T shlr(T value, int bits) noexcept
    {
        static_assert(std::is_integral_v<T>);
        return static_cast<T>(static_cast<std::make_unsigned_t<T>>(value) >> bits);
    }

    template <typename T>
    constexpr T shal(T value, int bits) noexcept
    {
        return shll<T>(value, bits);
    }

    template <typename T>
    constexpr T shar(T value, int bits) noexcept
    {
        static_assert(std::is_integral_v<T>);
        return static_cast<T>(static_cast<std::make_signed_t<T>>(value) >> bits);
    }

    template <typename T>
    constexpr int clz(T value) noexcept
    {
        static_assert(std::is_integral_v<T>);
        static_assert(std::is_unsigned_v<T>);
        static_assert(sizeof(T) >= sizeof(int));
        static_assert(sizeof(T) <= sizeof(long long));

#if defined(_MSC_VER)
        if constexpr (sizeof(T) == sizeof(long long))
        {
            return static_cast<int>(_lzcnt_u64(value));
        }
        else
        {
            return static_cast<int>(_lzcnt_u32(value));
        }
#else
        if (value != 0)
        {
            if constexpr (sizeof(T) == sizeof(long long))
            {
                return static_cast<int>(__builtin_clzll(value));
            }
            else
            {
                return static_cast<int>(__builtin_clz(value));
            }
        }

        return std::numeric_limits<T>::digits;
#endif
    }

    template <typename T>
    constexpr int ctz(T value) noexcept
    {
        static_assert(std::is_integral_v<T>);
        static_assert(sizeof(T) >= sizeof(int));
        static_assert(sizeof(T) <= sizeof(long long));

#if defined(_MSC_VER)
        if constexpr (sizeof(T) == sizeof(long long))
        {
            return static_cast<int>(_tzcnt_u64(value));
        }
        else
        {
            return static_cast<int>(_tzcnt_u32(value));
        }
#else
        if (value != 0)
        {
            if constexpr (sizeof(T) == sizeof(long long))
            {
                return static_cast<int>(__builtin_ctzll(value));
            }
            else
            {
                return static_cast<int>(__builtin_ctz(value));
            }
        }

        return std::numeric_limits<T>::digits;
#endif
    }

    template <typename T>
    constexpr int popcount(T value) noexcept
    {
#if defined(_MSC_VER)
        if constexpr (sizeof(T) == sizeof(long long))
        {
            return static_cast<int>(__popcnt64(value));
        }
        else
        {
            return static_cast<int>(__popcnt(value));
        }
#else
        if constexpr (sizeof(T) == sizeof(long long))
        {
            return static_cast<int>(__builtin_popcountll(value));
        }
        else
        {
            return static_cast<int>(__builtin_popcount(value));
        }
#endif
    }

    template <typename T>
    constexpr int msb(T value) noexcept
    {
        static_assert(std::is_integral_v<T>);
        static_assert(std::is_unsigned_v<T>);
        static_assert(std::numeric_limits<T>::radix == 2);

        return (value == 0) ? -1 : clz(value);
    }

    template <typename T>
    constexpr int lsb(T value) noexcept
    {
        static_assert(std::is_integral_v<T>);
        static_assert(std::is_unsigned_v<T>);
        return (value == 0) ? -1 : ctz(value);
    }

    template <typename T>
    constexpr int parity(T value) noexcept
    {
        value = value ^ shlr(value, 1);
        value = value ^ shlr(value, 2);
        value = value ^ shlr(value, 4);

        if constexpr (sizeof(T) > 1)
        {
            value = value ^ shlr(value, 8);

            if constexpr (sizeof(T) > 2)
            {
                value = value ^ shlr(value, 16);

                if constexpr (sizeof(T) > 4)
                {
                    value = value ^ shlr(value, 32);
                }
            }
        }

        return value;
    }
}


//
// Half type conversion.
//

namespace reland
{
    struct half
    {
        uint16_t value;
    };

    inline float from_half(half value) noexcept
    {
#if RELAND_HW_F16C
        __m128i v1 = _mm_cvtsi32_si128(static_cast<int32_t>(value.value));
        __m128 v2 = _mm_cvtph_ps(v1);
        return _mm_cvtss_f32(v2);
#else
        uint32_t mantissa = static_cast<uint32_t>(value.value & UINT32_C(0x03ff));
        uint32_t exponent = static_cast<uint32_t>(value.value & UINT32_C(0x7c00));

        if (exponent == UINT32_C(0x7c00))
        {
            exponent = UINT32_C(0x8f);
        }
        else if (exponent != 0)
        {
            exponent = static_cast<uint32_t>((value.value >> 10) & 0x1f);
        }
        else if (mantissa != 0)
        {
            exponent = 1;

            do
            {
                --exponent;
                mantissa <<= 1;
            } while ((mantissa & UINT32_C(0x0400)) == 0);

            mantissa &= UINT32_C(0x03ff);
        }
        else
        {
            exponent = static_cast<uint32_t>(-112);
        }

        uint32_t result = ((value.value & UINT32_C(0x8000)) << 16)
            | ((exponent + 112) << 23)
            | (mantissa << 13);

        ieee754_float pun;
        pun.as_uint = result;
        return pun.value;
#endif
    }

    inline half to_half(float value) noexcept
    {
#if RELAND_HW_F16C
        __m128 v1 = _mm_set_ss(value);
        __m128i v2 = _mm_cvtps_ph(v1, 0);
        return { static_cast<uint16_t>(_mm_cvtsi128_si32(v2)) };
#else
        ieee754_float pun;
        pun.value = value;

        uint32_t uvalue = pun.as_uint;
        uint32_t sign = (uvalue & UINT32_C(0x80000000)) >> UINT32_C(16);
        uvalue &= UINT32_C(0x7fffffff);

        uint32_t result;

        if (uvalue > UINT32_C(0x477fe000))
        {
            if (((uvalue & UINT32_C(0x7f800000)) == UINT32_C(0x7f800000)) && ((uvalue & UINT32_C(0x007fffff)) != 0))
            {
                result = UINT32_C(0x7fff);
            }
            else
            {
                result = UINT32_C(0x7c00);
            }
        }
        else
        {
            if (uvalue < UINT32_C(0x38800000))
            {
                uint32_t shift = UINT32_C(113) - (uvalue >> UINT32_C(23));
                uvalue = (UINT32_C(0x00800000) | (uvalue & UINT32_C(0x007fffff))) >> shift;
            }
            else
            {
                uvalue += UINT32_C(0xc8000000);
            }

            result = ((uvalue + UINT32_C(0x0fff) + ((uvalue >> UINT32_C(13)) & UINT32_C(1))) >> UINT32_C(13)) & UINT32_C(0x7fff);
        }

        return half{ static_cast<uint16_t>(result | sign) };
#endif
    }

}
