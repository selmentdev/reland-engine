#pragma once
#include <reland/module.core.hxx>

namespace reland::diagnostic
{
    enum struct error_report_mode
    {
        interactive,
        unattended,
    };

    enum struct assert_result
    {
        abort,
        retry,
        ignore,
        always_ignore,
    };

    struct debug
    {
        static void initialize() noexcept;
        static void shutdown() noexcept;

        static void handle_oom_failure(size_t allocation_size, size_t allocation_alignment) noexcept;
        static void handle_io_failure(std::string_view path) noexcept;
        static void handle_crash(::EXCEPTION_POINTERS* exception, ::DWORD code) noexcept;

        static assert_result show_assert_dialog(const char* title, const char* message) noexcept;
        static bool handle_assertion_failure(const char* expression, const char* file, unsigned int line, const char* format, ...) noexcept;

        static void write_string(const char* text) noexcept;
        static void write_format(const char* format, ...) noexcept;
        static void write_format_args(const char* format, va_list arglist) noexcept;
        static bool is_debugger_attached() noexcept;
    };
}
