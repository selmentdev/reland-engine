#pragma once
#include <reland/module.core.hxx>

namespace reland
{
    enum struct compression_method : uint32_t
    {
        lz4,
        lz4hc,
        zlib,
    };

    constexpr compression_method default_compression_method = compression_method::lz4;

    struct compression final
    {
        CORE_API static size_t memory_bound(
            compression_method method,
            size_t size
        ) noexcept;

        CORE_API static bool compress_block(
            compression_method method,
            void* output_buffer,
            size_t& output_size,
            const void* input_buffer,
            size_t input_size
        ) noexcept;

        CORE_API static bool decompress_block(
            compression_method method,
            void* output_buffer,
            size_t output_size,
            const void* input_buffer,
            size_t input_size
        ) noexcept;

        CORE_API static bool compress_block(
            compression_method method,
            std::vector<std::byte>& output,
            const std::vector<std::byte>& input
        ) noexcept;

        CORE_API static bool decompress_block(
            compression_method method,
            std::vector<std::byte>& output,
            const std::vector<std::byte>& input
        ) noexcept;

        CORE_API static bool compress_string(
            compression_method method,
            std::vector<std::byte>& output,
            std::string_view input
        );

        CORE_API static bool decompress_string(
            compression_method method,
            std::string& output,
            const std::vector<std::byte>& input
        );
    };
}
