project "core"
    language "C++"
    files {
        "public/**.hxx",
        "private/**.cxx",
        "**.lua"
    }
    pchheader "reland/module.core.hxx"
    pchsource "private/module.core.pch.cxx"
    includedirs {
        "%{wks.location}/engine/include",
        "./public",
    }
    defines {
        "module_core_EXPORTS=1"
    }

    filter { "system:not windows" }
        removefiles {
            "**/windows/**.cxx"
        }

    filter { "system:not linux" }
        removefiles {
            "**/linux/**.cxx"
        }

    dofile "sdk/libfmt/exports.lua"
