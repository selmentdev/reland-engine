#pragma once
#include <reland/module.entities.hxx>

namespace reland::entities
{
    struct entity final
    {
        union
        {
            uint64_t hash;
            struct
            {
                uint32_t index;
                uint32_t version;
            };
        };
    };
    static_assert(sizeof(entity) == sizeof(uint64_t));
}
