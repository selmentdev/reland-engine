#pragma once
#include <reland/module.entities.hxx>
#include <reland/entities/entity.hxx>

namespace reland::entities
{
    class ENTITIES_API entity_manager final
    {
    public:
        entity_manager() noexcept;
        ~entity_manager() noexcept;

    public:
        bool has_entity(entity e) const noexcept;
    };
}
