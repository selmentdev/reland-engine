#pragma once
#include <reland/detect.hxx>

#if RELAND_STATIC_BUILD
#define ENTITIES_API
#else
#if defined(module_entities_EXPORTS)
#define ENTITIES_API    __cxx_lib_export
#else
#define ENTITIES_API    __cxx_lib_import
#endif
#endif
