project "entities"
    language "C++"
    files {
        "public/**.hxx",
        "private/**.cxx",
        "**.lua"
    }
    pchheader "reland/module.entities.hxx"
    pchsource "private/module.entities.pch.cxx"
    includedirs {
        "%{wks.location}/engine/include",
        "./public",
    }
    defines {
        "module_entities_EXPORTS=1"
    }

    dofile "sdk/libfmt/exports.lua"
