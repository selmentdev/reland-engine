#include <reland/module.entities.hxx>
#include <reland/entities/entity_manager.hxx>

using namespace reland;
using namespace reland::entities;

reland::entities::entity_manager::entity_manager() noexcept = default;
reland::entities::entity_manager::~entity_manager() noexcept = default;

bool reland::entities::entity_manager::has_entity(reland::entities::entity e) const noexcept
{
    return e.hash != 0;
}
