#if __has_include(<reland/build.version.hxx>)
#include <reland/build.version.hxx>
#else
#define RELAND_BUILD_OS_VERSION    "<unknown>"
#define RELAND_BUILD_OS_HOST       "<unknown>"
#define RELAND_BUILD_COMMIT        "<unknown>"
#define RELAND_BUILD_COMMIT_SHORT  "<unknown>"
#define RELAND_BUILD_BRANCH        "<unknown>"
#define RELAND_BUILD_UUID          "<unknown>"
#endif

